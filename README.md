# MyNotes App

[![Netlify Status](https://api.netlify.com/api/v1/badges/64351447-0ced-4a6e-a8f8-a914b1f1816b/deploy-status)](https://app.netlify.com/sites/mynotes-mm/deploys)

## Just another over-engineered app for writing ~~todos~~ notes

Welcome everyone!. I made this app to learn about the different aspects of an Angular application, as well as the latest Architectural Patterns and good practices such as:
* Reactive programming with RxJS following a declarative approach
* State Management
* Error Handling
* Change Detection Optimizations
* Angular Testing (Unit and E2E Tests)
* Authentication with Azure AD

Feel free to check out the [DOCS](https://mynotes-mm.netlify.app/docs/introduction/) to learn more about this.

## Launch the application

Run `ng serve` or `npm start` for running a dev server, theb navigate to `http://localhost:4200/`.

<!-- ## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io). -->

This project uses [Spectator](https://ngneat.github.io/spectator/) as the main library for writing unit tests.

Spectator is an Angular unit testing library built on top of TestBed that helps to reduce boilerplate code, on top
of providing some extra nice to have features. It also integrates the **ng-mocks** library to easily mock dependencies.

Additionally the project uses [observer-spy](https://github.com/hirezio/observer-spy) to test observables.

## Running end-to-end tests

This project uses [Cypress](https://docs.cypress.io/) as the tool for e2e testing. To launch the Cypress desktop application run the command `npm run e2e`.
Alternatively run `ng serve` and `npm run cypress:open` in a different terminal window to launch the application, or `npm run cypress:run` to run the test in a headless browser, generating a video file and screenshots documenting the testing process.

## Run Linter and Formatter

Fix Lint and Formatting inconsistencies by running the command `ng lint --fix`. The project uses eslint, lint-staged, prettier and husky to keep the code base clean and consistent.
