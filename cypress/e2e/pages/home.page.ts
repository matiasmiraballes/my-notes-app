/**
 * Page object for the Home Page
 */

export class HomePage {
  public visit(): void {
    cy.visit('/')
  }

  public notes(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.byTestId('note')
  }

  public noteTitles(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.byTestId('note-title')
  }

  public noteDescriptions(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.byTestId('note-description')
  }

  public createNoteButton(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.byTestId('create-note-button')
  }

  public deleteNoteButton(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.byTestId('delete-note-button')
  }

  public notesFilterInput(): Cypress.Chainable<JQuery<HTMLElement>>{
    return cy.byTestId('notes-filter')
  }

  public clickOnBackground(): void {
    cy.get('body').click(0,0)
  }

  /**
   * Form for creating and editing notes
   */
  public noteEditorForm = {
    title: () => cy.byTestId('note-editor-title'),
    description: () => cy.byTestId('note-editor-description'),
    SharedWith: () => cy.byTestId('note-editor-sharedWith'),
  }

  public dataSourceOptions = {
    menu: () => cy.byTestId('data-source-menu'),
    dataSourceDropdown: () => cy.byTestId('data-source-dropdown'),
    submitButton: () => cy.byTestId('data-source-submit-button'),
  }
}
