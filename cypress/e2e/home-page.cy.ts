import { HomePage } from './pages/home.page'

describe('Home Page', () => {
  let page: HomePage

  beforeEach(() => {
    cy.login()
    page = new HomePage()
    page.visit()
  })

  it('Home Page loads', () => {
    cy.get('app-home')
  })

  it('Displays notes', () => {
    page.notes().its('length').should('be.gte', 1)
  })

  describe('Creates a notes submitting by', () => {
    let newNote = {
      title: '[E2E TEST] - Create Note 1',
      description: '[E2E TEST] - description for create note 1',
      sharedWith: '',
    }

    beforeEach(() => {
      // Open form and load parameters
      page.createNoteButton().click()
      page.noteEditorForm.title().first().clear().type(newNote.title)
      page.noteEditorForm
        .description()
        .first()
        .clear()
        .type(newNote.description)
    })

    it('clicking a note', () => {
      page.notes().first().click()
    })

    it('clicking on the background', () => {
      page.clickOnBackground()
    })

    afterEach(() => {
      // Assert
      page
        .noteTitles()
        .filter((_, element) => element.innerHTML.includes(newNote.title))
        .should('exist')
      page
        .noteDescriptions()
        .filter((_, element) => element.innerHTML.includes(newNote.description))
        .should('exist')
    })
  })

  describe('Updates a note submitting by -', () => {
    let editedValues = {
      title: '[E2E TEST] - Edited Note 1',
      description: '[E2E TEST] - description for edited note 1',
      sharedWith: '',
    }

    beforeEach(() => {
      // Open form and edit parameters
      page.notes().first().click()
      page.noteEditorForm.title().first().clear().type(editedValues.title)
      page.noteEditorForm
        .description()
        .first()
        .clear()
        .type(editedValues.description)
    })

    it('clicking a note', () => {
      page.notes().first().click()
    })

    it('clicking on the "create note button"', () => {
      page.createNoteButton().first().click()
    })

    it('clicking on the background', () => {
      page.clickOnBackground()
    })

    afterEach(() => {
      // Assert
      page
        .noteTitles()
        .filter((_, element) => element.innerHTML.includes(editedValues.title))
        .should('exist')
      page
        .noteDescriptions()
        .filter((_, element) =>
          element.innerHTML.includes(editedValues.description)
        )
        .should('exist')
    })
  })

  describe('delete note', () => {
    it('when clicking the delete button', () => {
      page
        .noteTitles()
        .first()
        .then($firstNoteTitleElement => {
          // Get the first's note title and delete the note
          const title = $firstNoteTitleElement.text()

          page
            .notes()
            .filter((_, element) => element.innerHTML.includes(title))
            .should('exist')
          page
            .notes()
            .filter((_, element) => element.innerHTML.includes(title))
            .click()

          page.deleteNoteButton().click()

          page
            .notes()
            .filter((_, element) => element.innerHTML.includes(title))
            .should('not.exist')
        })
    })
  })

  describe('filter note', () => {
    it('should display notes with matching title', () => {
      page.noteTitles().then($value => {
        const firstNoteTitle = $value[0].textContent
        const secondNoteTitle = $value[1].textContent

        page
          .noteTitles()
          .filter((_, element) => element.innerHTML.includes(firstNoteTitle))
          .should('exist')
        page
          .noteTitles()
          .filter((_, element) => element.innerHTML.includes(secondNoteTitle))
          .should('exist')

        page.notesFilterInput().first().clear().type(firstNoteTitle)

        page
          .noteTitles()
          .filter((_, element) => element.innerHTML.includes(firstNoteTitle))
          .should('exist')
        page
          .noteTitles()
          .filter((_, element) => element.innerHTML.includes(secondNoteTitle))
          .should('not.exist')
      })
    })
  })

  describe('Data source options', () => {
    it('should display different amount of data when data source changes', () => {
      page.noteTitles().then($initialData => {
        let initialAmountOfNotes = $initialData.length

        page.dataSourceOptions.menu().click()
        page.dataSourceOptions.dataSourceDropdown().select(1)

        page.dataSourceOptions.submitButton().click()

        page.notes().should('not.have.length', initialAmountOfNotes)

        page.clickOnBackground()
      })
    })
  })
})

// describe('… Feature description …', () => {
//   beforeEach(() => {
//     // Navigate to the page
//   });

//   it('… User interaction description …', () => {
//     // Interact with the page
//     // Assert something about the page content
//   });
// });

// function getNoteByTitle(noteTitle: string) {
//   let foundNoteHTMLElement = Array.from(document.querySelectorAll(`[data-testid="note"]`))
//     .filter((element) => element.innerHTML.includes(noteTitle))[0]

//   return {
//     title: findChildByTestId(foundNoteHTMLElement, "note-title").innerText,
//     description: findChildByTestId(foundNoteHTMLElement, "note-description").innerText
//   }
// }

// function findChildByTestId(root, testId) {
//   if (root.childNodes.length == 0) return null

//   let queue = Array.from(root.children) as HTMLElement[]
//   while (queue.length > 0) {
//     let currentElement = queue.shift()
//     if (currentElement.dataset['testid'] == testId)
//       return currentElement

//     if (currentElement.childNodes.length > 0) {
//       Array.from(currentElement.children).forEach((child: HTMLElement) => {
//         queue.push(child)
//       })
//     }
//   }

//   return null
// }
