import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core'
import { SvgIconRegistryService } from 'angular-svg-icon'
import { SvgMapper } from '../core/utils/svg/svg-mapper'
import { SettingsStore } from '../core/settings/settings.store'
import { Subscription, take } from 'rxjs'
import { SubSink } from 'subsink'
import { hexToHsl } from '../core/utils/css/css-helpers'
import { LoggerService } from '../core/http/services/logger.service'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-root',
  template: `
    <!DOCTYPE html>
    <html lang="en">
      <app-layout *ngIf="stylesLoaded"></app-layout>
    </html>
  `,
  styles: [],
})
export class AppComponent implements OnInit, OnDestroy {
  stylesLoaded = false
  themeSubscription: Subscription
  private subs = new SubSink()

  constructor(
    private iconReg: SvgIconRegistryService,
    private settingsStore: SettingsStore,
    private loggerService: LoggerService
  ) {}

  ngOnInit(): void {
    this.loggerService.logActivity('User entered home page')
    this.mapSvgFiles()
    this.loadSelectedTheme()
  }

  private mapSvgFiles() {
    new SvgMapper().getSvgData().forEach(svg => {
      this.subs.sink = this.iconReg
        .loadSvg(svg.path, svg.name)
        .pipe(take(1))
        .subscribe()
    })
  }

  private loadSelectedTheme() {
    this.subs.sink = this.settingsStore
      .getSelectedTheme()
      .subscribe(selectedTheme => {
        this.setCssGammaColor('primary', hexToHsl(selectedTheme.colors.primary))
        this.setCssGammaColor(
          'secondary',
          hexToHsl(selectedTheme.colors.secondary)
        )
        this.setCssGammaColor(
          'tertiary',
          hexToHsl(selectedTheme.colors.tertiary)
        )
        this.setCssGammaColor(
          'background',
          hexToHsl(selectedTheme.colors.background)
        )

        if (!this.stylesLoaded) this.stylesLoaded = true
      })
  }

  private setCssGammaColor(
    colorIdentifier,
    { color, colorDarker, colorDarkest }
  ) {
    this.setRootCssVariable(`--color-${colorIdentifier}`, color)
    this.setRootCssVariable(`--color-${colorIdentifier}-darker`, colorDarker)
    this.setRootCssVariable(`--color-${colorIdentifier}-darkest`, colorDarkest)
  }

  private setRootCssVariable(variableName, value) {
    document.documentElement.style.setProperty(variableName, value)
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }
}
