import { ChangeDetectionStrategy, Component } from '@angular/core'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-layout',
  template: `
    <nav>
      <app-navbar></app-navbar>
    </nav>

    <main>
      <router-outlet></router-outlet>
    </main>

    <footer>
      <hr />
      <p>©Copyleft 2050 by Matias Miraballes. All rights reversed.</p>
    </footer>

    <!-- Bootstrap Javascript -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
      crossorigin="anonymous"></script>
  `,
  styles: [
    `
      body {
        min-height: 100vh;
        display: flex;
        flex-direction: column;
      }

      main {
        margin-top: 1rem;
      }

      footer {
        padding-top: 5px;
        margin-top: auto;

        & > p {
          padding-left: 2%;
        }
      }
    `,
  ],
})
export class MainLayoutComponent {
  //https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Document_and_website_structure
}
