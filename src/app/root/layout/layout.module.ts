import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MainLayoutComponent } from './main-layout.component'
import { RouterModule } from '@angular/router'
import { ReactiveFormsModule } from '@angular/forms'
import { DirectivesModule } from 'src/app/core/directives/directives.module'
import { SharedModule } from 'src/app/modules/shared/shared.module'
import { NavbarComponent } from 'src/app/modules/navbar/navbar.component'
import { ThemePickerMenuComponent } from 'src/app/modules/navbar/theme-picker-menu/theme-picker-menu.component'
import { ThemePickerOptionComponent } from 'src/app/modules/navbar/theme-picker-menu/theme-picker-option.component'
import { DataSourceMenuComponent } from 'src/app/modules/navbar/data-source-menu/data-source-menu.component'
import { DataSourceFormComponent } from 'src/app/modules/navbar/data-source-menu/data-source-form.component'

@NgModule({
  declarations: [
    MainLayoutComponent,
    NavbarComponent,
    ThemePickerMenuComponent,
    ThemePickerOptionComponent,
    DataSourceMenuComponent,
    DataSourceFormComponent,
  ],
  exports: [MainLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    DirectivesModule,
    SharedModule,
  ],
})
export class LayoutModule {}
