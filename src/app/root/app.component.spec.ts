import { AppComponent } from './app.component'
import { MainLayoutComponent } from './layout/main-layout.component'
import { Spectator, createComponentFactory } from '@ngneat/spectator'
import { MockComponent, MockProvider } from 'ng-mocks'
import { SvgIconRegistryService } from 'angular-svg-icon'
import { SettingsStore } from '../core/settings/settings.store'
import { of } from 'rxjs'
import defaultThemes from '../core/settings/themes.json'
import { Theme } from '../core/settings/models/style-settings.models'
import { LoggerService } from '../core/http/services/logger.service'

let selectedTheme: Theme = defaultThemes[0]

describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>

  const createComponent = createComponentFactory({
    component: AppComponent,
    declarations: [MockComponent(MainLayoutComponent)],
    providers: [
      MockProvider(SvgIconRegistryService, {
        loadSvg: () => of(null),
      }),
      MockProvider(SettingsStore, {
        getSelectedTheme: () => of(selectedTheme),
      }),
      MockProvider(LoggerService, {
        logActivity: (message: string) => null,
      }),
    ],
  })

  beforeEach(() => {
    spectator = createComponent()
  })

  it('should create component', () => {
    const app = spectator.component
    expect(app).toBeTruthy()
  })
})
