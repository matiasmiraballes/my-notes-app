import { AuthService } from '../../../../core/auth/auth.service'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Observable } from 'rxjs'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'profile-claims',
  template: ` <div class="container">
    <div class="authenticating-loader" *ngIf="identityClaims !== null">
      <h2>
        HI {{ identityClaims.preferred_username }}, THANKS FOR LOGGING INTO THE
        APPLICATION!
      </h2>

      <h4>The following information is needed for running E2E tests</h4>
      <h5>Access Token:</h5>
      <code class="break-all">{{ accessToken }}</code>

      <h5>Id Token:</h5>
      <code class="break-all">{{ idToken }}</code>
    </div>
  </div>`,
  styles: [],
})
export class ClaimsComponent {
  isAuthenticated: Observable<boolean>
  isDoneLoading: Observable<boolean>
  canActivateProtectedRoutes: Observable<boolean>

  constructor(private authService: AuthService) {
    this.isAuthenticated = this.authService.isAuthenticated$
    this.isDoneLoading = this.authService.isDoneLoading$
    this.canActivateProtectedRoutes =
      this.authService.canActivateProtectedRoutes$
  }

  get hasValidToken() {
    return this.authService.hasValidToken()
  }
  get accessToken() {
    return this.authService.accessToken
  }
  get refreshToken() {
    return this.authService.refreshToken
  }
  get identityClaims() {
    return this.authService.identityClaims
  }
  get idToken() {
    return this.authService.idToken
  }
}
