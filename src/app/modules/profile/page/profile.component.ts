import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-profile',
  template: `<profile-claims></profile-claims>`,
})
export class ProfileComponent {
  constructor() {}
}
