import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ClaimsComponent } from './components/claims/claims.component'
import { ProfileComponent } from './page/profile.component'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
]

@NgModule({
  declarations: [ProfileComponent, ClaimsComponent],
  imports: [RouterModule.forChild(routes), CommonModule],
})
export class ProfileModule {}
