import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core'
import { FormBuilder } from '@angular/forms'
import {
  DEFAULT_CUSTOM_THEME_ID,
  Theme,
} from 'src/app/core/settings/models/style-settings.models'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'layout-theme-picker-option',
  template: `
    <div class="navbar-dropdown-item" (click)="clickTheme($event, theme.id)">
      <div>{{ isSelected ? '✔️' : '' }}</div>
      <div>{{ theme.description }}</div>
      <div>
        <form [formGroup]="themeForm">
          <!-- (change) event makes the input close immediately due to making the component rerender when the input changes -->
          <input
            class="color-display"
            type="color"
            formControlName="primary"
            (change)="submitCustomTheme()" />
          <input
            class="color-display"
            type="color"
            formControlName="secondary"
            (change)="submitCustomTheme()" />
          <input
            class="color-display"
            type="color"
            formControlName="tertiary"
            (change)="submitCustomTheme()" />
          <input
            class="color-display"
            type="color"
            formControlName="background"
            (change)="submitCustomTheme()" />
        </form>
      </div>
    </div>
  `,
  styles: [
    `
      .navbar-dropdown-item {
        display: flex;
        width: 100%;
        height: 2rem;
        align-items: center;
        justify-content: flex-end;
        gap: 1rem;
        padding-right: 10px;

        &:hover {
          backdrop-filter: contrast(0.8);
        }
      }

      input[type='color'] {
        width: 1.15rem;
        height: 1.15rem;
        border-radius: 50%;
        overflow: hidden;
        border: 1px var(--color-primary) solid;
        background-color: var(--color-primary);
        margin-left: 3px;
      }

      input[type='color']::-webkit-color-swatch {
        border: none;
        border-radius: 50%;
        padding: 0;
      }

      input[type='color']::-webkit-color-swatch-wrapper {
        border: none;
        border-radius: 50%;
        padding: 0;
      }

      input[type='color']::-moz-color-swatch {
        border-radius: 50%;
        border-style: none;
      }
    `,
  ],
})
export class ThemePickerOptionComponent implements OnInit {
  @Input() theme: Theme
  @Input() colorPickerEnabled: boolean
  @Input() isSelected: boolean = true

  @Output() themeClicked = new EventEmitter<number>()
  @Output() themeSubmited = new EventEmitter<Theme>()

  themeForm = this.fb.group({
    primary: '#ffffff',
    secondary: '#ffffff',
    tertiary: '#ffffff',
    background: '#ffffff',
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.themeForm = this.fb.group({
      primary: {
        value: this.theme.colors.primary,
        disabled: !this.colorPickerEnabled,
      },
      secondary: {
        value: this.theme.colors.secondary,
        disabled: !this.colorPickerEnabled,
      },
      tertiary: {
        value: this.theme.colors.tertiary,
        disabled: !this.colorPickerEnabled,
      },
      background: {
        value: this.theme.colors.background,
        disabled: !this.colorPickerEnabled,
      },
    })
  }

  clickTheme($event, themeId) {
    let colorPickedWasClicked =
      Array.from($event.target.classList).find(c => c == 'color-display') !=
      undefined
    if (colorPickedWasClicked && this.colorPickerEnabled) return

    this.themeClicked.emit(themeId)
  }

  submitCustomTheme() {
    if (!this.colorPickerEnabled) return

    if (!this.themeForm.valid) return

    let newTheme: Theme = {
      id: DEFAULT_CUSTOM_THEME_ID,
      description: 'Customize Theme...',
      colors: {
        primary: this.themeForm.value.primary,
        secondary: this.themeForm.value.secondary,
        tertiary: this.themeForm.value.tertiary,
        background: this.themeForm.value.background,
      },
    }
    this.themeSubmited.emit(newTheme)
  }
}
