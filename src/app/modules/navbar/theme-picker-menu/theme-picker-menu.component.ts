import { ChangeDetectionStrategy, Component } from '@angular/core'
import { delay, first, map, Observable } from 'rxjs'
import {
  DEFAULT_CUSTOM_THEME_ID,
  StyleSettings,
  Theme,
} from 'src/app/core/settings/models/style-settings.models'
import { SettingsStore } from 'src/app/core/settings/settings.store'

export interface ThemeViewModel {
  theme: Theme
  isCustomTheme: boolean
  isSelected: boolean
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'layout-theme-picker-menu',
  template: `
    <div clickOutside (clickOutside)="clickedOutside()">
      <div class="dropdown-menu-button" (click)="toggleMenu()">
        <shared-svg-icon
          [svgName]="'color-editor-3'"
          [leftLabel]="'Theme Picker Menu'"></shared-svg-icon>
      </div>
      <div class="navbar-dropdown-menu dropdown-menu" *ngIf="displayMenu">
        <ng-container *ngFor="let themeVm of newThemeViewModel$ | async">
          <layout-theme-picker-option
            [theme]="themeVm.theme"
            [colorPickerEnabled]="themeVm.isCustomTheme"
            [isSelected]="themeVm.isSelected"
            (themeClicked)="selectTheme($event)"
            (themeSubmited)="
              submitCustomTheme($event)
            "></layout-theme-picker-option>
        </ng-container>
      </div>
    </div>
  `,
  styles: [
    `
      .navbar-dropdown-menu {
        display: flex;
        flex-direction: column;
        position: absolute;
        top: 2.4em;
        width: 18rem;
        transform: translateX(-30%);
        background-color: var(--color-secondary);
        color: var(--color-tertiary);
        align-items: center;
        z-index: 1000;
        border-radius: 0.25rem;
      }

      layout-theme-picker-option {
        width: 100%;
      }

      .dropdown-menu-button {
        color: var(--color-tertiary);
        padding: 0px 10px 0px 10px;

        &:hover {
          border-radius: 10px;
          backdrop-filter: contrast(0.8);
        }

        &:active {
          border-radius: 10px;
          backdrop-filter: contrast(0.6);
        }
      }
    `,
  ],
})
export class ThemePickerMenuComponent {
  displayMenu = false
  newThemeViewModel$: Observable<ThemeViewModel[]> =
    this.settingsStore.styleSettings$.pipe(
      // first(),
      map(settings => {
        return this.buildViewModel(settings)
      })
    )

  constructor(private settingsStore: SettingsStore) {}

  private buildViewModel(settings: StyleSettings): ThemeViewModel[] {
    let selectedTheme = settings.theme.selectedTheme

    let predefinedThemesViewModel: ThemeViewModel[] =
      settings.theme.predefinedThemes.map(t => {
        return {
          theme: t,
          isSelected: t.id == selectedTheme,
          isCustomTheme: false,
        }
      })

    let customThemesViewModel: ThemeViewModel[] =
      settings.theme.customThemes.map(t => {
        return {
          theme: t,
          isSelected: t.id == selectedTheme,
          isCustomTheme: true,
        }
      })

    return [...predefinedThemesViewModel, ...customThemesViewModel]
  }

  submitCustomTheme(customTheme: Theme) {
    this.settingsStore.styleSettings$
      .pipe(
        first(),
        map(currentSettings => {
          currentSettings.theme.selectedTheme = DEFAULT_CUSTOM_THEME_ID
          currentSettings.theme.customThemes = [customTheme]

          return currentSettings
        })
      )
      .subscribe(newSettings => {
        this.settingsStore.updateThemesSettings(newSettings)
      })
  }

  selectTheme(themeId: number) {
    this.settingsStore.styleSettings$
      .pipe(
        first(),
        map(currentSettings => {
          currentSettings.theme.selectedTheme = themeId
          return currentSettings
        }),
        delay(0) // Wait until next UI cycle before tapping new the value to avoid issues with "clickOutside" check
      )
      .subscribe(newSettings => {
        this.settingsStore.updateThemesSettings(newSettings)
      })
  }

  toggleMenu() {
    this.displayMenu = !this.displayMenu
  }

  clickedOutside() {
    this.displayMenu = false
  }
}
