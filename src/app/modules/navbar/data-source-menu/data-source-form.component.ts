import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core'
import { FormBuilder, Validators } from '@angular/forms'
import { DataSourceSettings } from 'src/app/core/settings/models/data-source-settings.model'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'layout-data-source-form',
  template: `
    <form
      [formGroup]="dataSourceForm"
      (ngSubmit)="submitForm()"
      class="datasource-form">
      <div class="form-option">
        <div class="label-container">
          <label>Data Source</label>
        </div>
        <div class="option-container">
          <select data-testid="data-source-dropdown" formControlName="source">
            <option
              *ngFor="let source of dataSourceSettings.sources"
              [ngValue]="source.id">
              {{ source.description }}
            </option>
          </select>
        </div>
      </div>

      <div class="form-option">
        <div class="label-container">
          <label>Delay</label>
        </div>
        <div class="option-container">
          <input
            type="range"
            formControlName="delayInMs"
            min="0"
            max="1000"
            step="100" />
          {{ dataSourceForm.value.delayInMs + 'ms' }}
        </div>
      </div>

      <div class="form-option">
        <div class="label-container">
          <label>Unstable Connection</label>
        </div>
        <div class="option-container">
          <input type="checkbox" formControlName="unstableConnection" />
        </div>
      </div>

      <button type="submit" data-testid="data-source-submit-button">
        Apply Options
      </button>
    </form>
  `,
  styles: [
    `
      .datasource-form {
        display: flex;
        flex-direction: column;
        width: 100%;
      }

      .form-option {
        display: grid;
        grid-template-columns: 1fr 2fr;
        grid-gap: 10px;
        height: 2.5rem;
        width: 100%;

        & > .label-container {
          display: flex;
          justify-content: flex-end;
          align-items: center;
        }
        & > .option-container {
          display: flex;
          justify-content: flex-start;
          align-items: center;
        }
      }

      select {
        width: 90%;
      }

      button {
        background-color: var(--color-tertiary);
        color: var(--color-primary);
        border-radius: 3px;
        transition: var(--animation-transition-speed);
        height: 2.75rem;

        &:hover {
          background-color: var(--color-tertiary-darker);
        }

        &:active {
          background-color: var(--color-tertiary-darkest);
        }
      }
    `,
  ],
})
export class DataSourceFormComponent implements OnInit {
  @Input() dataSourceSettings: DataSourceSettings
  @Output() sourceSettingsSubmited = new EventEmitter<DataSourceSettings>()

  dataSourceForm = this.fb.group({
    source: 0,
    delayInMs: 0,
    unstableConnection: false,
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    let selectedSource = this.dataSourceSettings.sources.find(
      s => s.id == this.dataSourceSettings.selectedSource
    )
    this.dataSourceForm = this.fb.group({
      source: [selectedSource.id, Validators.required],
      delayInMs: [
        this.dataSourceSettings.options.delayInMs,
        [Validators.required, Validators.min(0), Validators.max(1000)],
      ],
      unstableConnection: [
        this.dataSourceSettings.options.unstableConnection,
        Validators.required,
      ],
    })
  }

  submitForm() {
    if (!this.dataSourceForm.valid) return

    let newSettings = {
      sources: this.dataSourceSettings.sources,
      selectedSource: this.dataSourceForm.value.source,
      options: {
        delayInMs: this.dataSourceForm.value.delayInMs,
        unstableConnection: this.dataSourceForm.value.unstableConnection,
      },
    }
    this.sourceSettingsSubmited.emit(newSettings)
  }
}
