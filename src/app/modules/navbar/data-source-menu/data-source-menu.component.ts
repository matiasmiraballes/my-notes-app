import { ChangeDetectionStrategy, Component } from '@angular/core'
import { LoggerService } from 'src/app/core/http/services/logger.service'
import { DataSourceSettings } from 'src/app/core/settings/models/data-source-settings.model'
import { SettingsStore } from 'src/app/core/settings/settings.store'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'layout-data-source-menu',
  template: `
    <div clickOutside (clickOutside)="clickedOutside()">
      <div class="dropdown-menu-button" (click)="toggleMenu()">
        <shared-svg-icon
          [svgName]="'settings-control-1'"
          [leftLabel]="'Data Source Options'"></shared-svg-icon>
      </div>
      <div class=" navbar-dropdown-menu dropdown-menu" *ngIf="displayMenu">
        <layout-data-source-form
          [dataSourceSettings]="settingsStore.dataSourceSettings$ | async"
          (sourceSettingsSubmited)="
            updateSettings($event)
          "></layout-data-source-form>
      </div>
    </div>
  `,
  styles: [
    `
      .navbar-dropdown-menu {
        display: flex;
        position: absolute;
        flex-direction: column;
        top: 2.4em;
        width: 24rem;
        transform: translateX(-30%);
        background-color: var(--color-secondary);
        color: var(--color-tertiary);
        align-items: center;
        z-index: 1000;
        border-radius: 0.25rem;
        padding-bottom: 0px;
      }

      .dropdown-menu-button {
        color: var(--color-tertiary);
        padding: 0px 10px 0px 10px;

        &:hover {
          border-radius: 10px;
          backdrop-filter: contrast(0.8);
        }

        &:active {
          border-radius: 10px;
          backdrop-filter: contrast(0.6);
        }
      }
    `,
  ],
})
export class DataSourceMenuComponent {
  displayMenu = false

  constructor(
    public settingsStore: SettingsStore,
    private loggerService: LoggerService
  ) {}

  toggleMenu() {
    this.displayMenu = !this.displayMenu
  }

  clickedOutside() {
    this.displayMenu = false
  }

  updateSettings(newSettings: DataSourceSettings) {
    this.loggerService.logActivity('User has updated datasource settings')
    this.settingsStore.updateDataSourceSettings(newSettings)
  }
}
