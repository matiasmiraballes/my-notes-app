import { ChangeDetectionStrategy, Component } from '@angular/core'
import { LoggerService } from 'src/app/core/http/services/logger.service'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-navbar',
  template: `
    <div class="container-fluid navbar-container">
      <a class="navbar-brand" href="#">MyNotes</a>

      <div class="navbar-section navbar-section-links">
        <a class="" href="/docs/" target="_blank" (click)="logAction()">DOCS</a>
        <a class="" href="/profile/">Profile</a>
        <!-- <a class="" href="#">Contacts</a> -->
      </div>

      <div class="spacer"></div>

      <div class="navbar-section navbar-section-configuration">
        <layout-data-source-menu
          data-testid="data-source-menu"></layout-data-source-menu>
        <layout-theme-picker-menu></layout-theme-picker-menu>
      </div>
    </div>
  `,
  styles: [
    `
      @mixin link-style {
        text-decoration: none;
        color: var(--color-tertiary);

        &:hover {
          color: var(--color-tertiary);
        }
      }

      .navbar-container {
        display: grid;
        grid-template-columns: fit-content(100px) 3fr auto 2fr;
        background-color: var(--color-secondary);
        height: 2.8em;
      }

      a:not(.navbar-brand) {
        @include link-style;
        padding: 0px 10px 0px 10px;

        &:hover {
          border-radius: 10px;
          backdrop-filter: contrast(0.8);
        }
      }

      .navbar-brand {
        @include link-style;
      }

      .navbar-section {
        display: flex;
        justify-content: flex-start;
        column-gap: 15px;
        align-items: center;
        white-space: nowrap;
      }

      .navbar-section-configuration {
        display: flex;
        justify-content: flex-end;
      }
    `,
  ],
})
export class NavbarComponent {
  constructor(private loggerService: LoggerService) {}

  logAction() {
    this.loggerService.logActivity('Used navigated to DOCS')
  }
}
