import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { PublicNote } from 'src/app/core/models/public-note.model'
import { NoteFrameComponent } from '../note-frame/note-frame.component'
import { UserPillBadgeComponent } from '../user-pill-badge/user-pill-badge.component'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-shared-note',
  template: `
    <ui-note-frame [hasOnHoverEffects]="false">
      <div data-testid="shared-note" class="card mb-3 note-container">
        <div class="card-body">
          <p data-testid="shared-note-title" class="card-title">
            {{ sharedNote.title }}
          </p>
          <p
            data-testid="shared-note-description"
            class="card-text note-description">
            {{ sharedNote.description }}
          </p>
          <p data-testid="shared-note-owner" class="card-text note-description">
            <b>Note owner:</b> {{ sharedNote.owner.email }}
          </p>
          <div class="card-text">
            <ng-container *ngFor="let sharedWith of sharedNote.sharedWith">
              <ui-user-pill-badge
                [email]="sharedWith"
                [hasDelete]="false"></ui-user-pill-badge>
            </ng-container>
          </div>
        </div>
      </div>
    </ui-note-frame>
  `,
  styles: [
    `
      .card {
        background-color: var(--color-background);
        height: 100%;
      }

      .note-description {
        flex-grow: 1;
      }

      .card-body {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
      }
    `,
  ],
  imports: [NoteFrameComponent, UserPillBadgeComponent, CommonModule],
})
export class SharedNoteComponent {
  @Input() sharedNote: PublicNote
}
