import { CommonModule } from '@angular/common'
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-user-suggestions-dropdown',
  template: ` <div id="suggestion-root" *ngIf="suggestions?.length > 0">
    <div id="suggestions-container">
      <span
        *ngFor="let suggestion of suggestions"
        (click)="onSuggestionClicked(suggestion)"
        >{{ suggestion }}</span
      >
    </div>
  </div>`,
  styles: [
    `
      #suggestion-root {
        position: relative;

        & #suggestions-container {
          display: flex;
          flex-direction: column;
          position: absolute;
          width: 18rem;
          background-color: var(--color-secondary);
          color: var(--color-tertiary);
          align-items: end;
          z-index: 1000;
        }

        &:hover {
          cursor: pointer;
        }
      }

      span {
        width: 100%;
        padding: 0px 10px 0px 10px;

        &:hover {
          background-color: var(--color-secondary-darker);
        }
      }
    `,
  ],
  imports: [CommonModule],
})
export class UserSuggestionsDropdownComponent {
  @Input() suggestions: string[]
  @Output() suggestionClicked = new EventEmitter<string>()

  onSuggestionClicked(suggestionClicked: string) {
    setTimeout(() => {
      this.suggestionClicked.emit(suggestionClicked)
    }, 0)
  }
}
