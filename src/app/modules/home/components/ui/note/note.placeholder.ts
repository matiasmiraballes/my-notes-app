import { ChangeDetectionStrategy, Component } from '@angular/core'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'home-note-placeholder',
  template: `
    <div class="container loading">
      <div class="content">
        <div class="stripe medium-stripe title"></div>
        <div class="stripe long-stripe"></div>
        <div class="stripe medium-stripe title"></div>
        <div class="stripe long-stripe"></div>
        <div class="stripe long-stripe"></div>
        <div class="stripe long-stripe"></div>
        <div class="stripe small-stripe"></div>
      </div>
    </div>
  `,
  styles: [
    `
      @use 'mixins' as m;

      .container {
        @include m.note-container-size;
        display: flex;
        background-color: var(--color-secondary);
        padding: 1%;
        border: 1px solid var(--color-tertiary);
        border-radius: 10px;
      }

      .title {
        margin-bottom: 10px;
      }

      .content {
        border: 1px solid var(--color-tertiary);
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        padding: 20px;
        justify-content: space-between;
      }

      .stripe {
        border: 1px solid var(--color-tertiary);
        height: 15%;
        background-color: var(--color-primary);
      }

      .small-stripe {
        width: 40%;
      }

      .medium-stripe {
        width: 70%;
      }

      .long-stripe {
        width: 100%;
      }

      .container.loading .img,
      .container.loading .stripe {
        animation: hintloading 2s ease-in-out 0s infinite reverse;
        -webkit-animation: hintloading 2s ease-in-out 0s infinite reverse;
      }

      @keyframes hintloading {
        0% {
          opacity: 0.5;
        }
        50% {
          opacity: 1;
        }
        100% {
          opacity: 0.5;
        }
      }

      @-webkit-keyframes hintloading {
        0% {
          opacity: 0.5;
        }
        50% {
          opacity: 1;
        }
        100% {
          opacity: 0.5;
        }
      }
    `,
  ],
})
export class NotePlaceholderComponent {}
