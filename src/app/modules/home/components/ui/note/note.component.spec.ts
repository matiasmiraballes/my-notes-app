import { byTestId, createComponentFactory, Spectator } from '@ngneat/spectator'
import { MockDb } from 'src/app/core/mocks/mock-db/mock-db'

import { Note } from 'src/app/core/models/note.model'
import { NoteComponent } from './note.component'
import { fakeAsync, tick } from '@angular/core/testing'

let mockNote: Note = new MockDb().Testing.getUserNoteSingle()
class NoteComponentPage {
  constructor(private spectator: Spectator<NoteComponent>) {
    this.spectator = spectator
  }

  public title() {
    return this.spectator.query(byTestId('note-title'))
  }

  public description() {
    return this.spectator.query(byTestId('note-description'))
  }

  public componentContainer() {
    return this.spectator.query(byTestId('note'))
  }
}

describe('NoteComponent', () => {
  let spectator: Spectator<NoteComponent>
  let page: NoteComponentPage

  const createComponent = createComponentFactory({
    component: NoteComponent,
    shallow: true,
  })

  beforeEach(() => {
    spectator = createComponent({ props: { note: mockNote } })
    page = new NoteComponentPage(spectator)
  })

  it('should render the properties of the note received as input', () => {
    expect(page.title()).toHaveText(mockNote.title)

    expect(page.description()).toHaveText(mockNote.description)
  })

  it('should emit note properties when clicked', fakeAsync(() => {
    let inputNote = { ...mockNote }
    let noteEmitedOnClick: Note | undefined
    spectator.component.noteClicked.subscribe((note: Note) => {
      noteEmitedOnClick = note
    })

    // Wait 1ms after clicking due to the setTimeout workaround
    spectator.click(page.componentContainer())
    tick(1)

    expect(noteEmitedOnClick).toEqual(inputNote)
  }))
})
