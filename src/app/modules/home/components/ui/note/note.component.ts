import { CommonModule } from '@angular/common'
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core'
import { Note } from 'src/app/core/models/note.model'
import { NoteFrameComponent } from '../note-frame/note-frame.component'
import { UserPillBadgeComponent } from '../user-pill-badge/user-pill-badge.component'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-note',
  template: `
    <ui-note-frame>
      <div
        data-testid="note"
        class="card mb-3 note-container"
        (click)="onClick()">
        <div class="card-body">
          <p data-testid="note-title" class="card-title">{{ note.title }}</p>
          <div class="description-container">
            <p
              data-testid="note-description"
              class="card-text note-description">
              {{ note.description }}
            </p>
          </div>
          <div class="card-text shared-with-container">
            <ng-container
              *ngFor="let sharedWith of note.sharedWith; index as i">
              <hr class="hr hr-blurry m-2" *ngIf="i === 0" />
              <ui-user-pill-badge
                [email]="sharedWith"
                [hasDelete]="false"></ui-user-pill-badge>
            </ng-container>
          </div>
        </div>
      </div>
    </ui-note-frame>
  `,
  styles: [
    `
      .card {
        background-color: var(--color-background);
        height: 100%;
      }

      ::-webkit-scrollbar {
        width: 0.7rem;
      }

      ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 4px var(--color-primary);
        border-radius: 10px;
      }

      ::-webkit-scrollbar-thumb {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 10px var(--color-primary);
      }

      @supports (scrollbar-color: red blue) {
        * {
          scrollbar-color: var(--color-primary);
          scrollbar-width: thin;
        }
      }

      .description-container {
        overflow: auto;
        flex-grow: 1;
      }

      .card-title {
        flex-basis: 2rem;
      }

      .shared-with-container {
        flex-basis: 2rem;
      }

      .card-body {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        height: 100%;
      }
    `,
  ],
  imports: [UserPillBadgeComponent, NoteFrameComponent, CommonModule],
})
export class NoteComponent {
  @Input() note: Note
  @Output() noteClicked = new EventEmitter<Note>()

  constructor() {}

  onClick() {
    // wait for a DOM cycle to avoid issues when submitting changes on note editor
    setTimeout(() => {
      this.noteClicked.emit(this.note)
    }, 0)
  }
}
