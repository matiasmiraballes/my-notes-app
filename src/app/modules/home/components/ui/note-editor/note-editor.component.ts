import { NoteEditorAction } from './note-editor-action'
import { Note } from 'src/app/core/models/note.model'
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { UsersService } from 'src/app/core/http/services/users.service'
import {
  BehaviorSubject,
  combineLatest,
  debounceTime,
  iif,
  map,
  Observable,
  of,
  startWith,
  switchMap,
  tap,
} from 'rxjs'
import { deepCopy, isDeepEqual } from 'src/app/core/utils/shared/functions'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-note-editor',
  template: `
    <form
      data-testid="note-editor-form"
      [formGroup]="formEdit"
      clickOutside
      (clickOutside)="onClickOutsideEvent()">
      <div class="top-row">
        <input
          data-testid="note-editor-title"
          class="input"
          type="text"
          formControlName="title"
          placeholder="Title*" />
        <div
          *ngIf="showUIButtons"
          class="svg-container"
          (click)="onDeleteNote()">
          <svg-icon
            name="delete-item-bin-2"
            [class]="'svg-icon'"
            data-testid="delete-note-button"
            id="btnDeleteNote">
          </svg-icon>
        </div>
      </div>
      <textarea
        data-testid="note-editor-description"
        class="input"
        type="text"
        formControlName="description"
        placeholder="Description"></textarea>
      <div id="sharedWith-container">
        <ui-user-pill-badge
          *ngFor="let sharedWith of sharedWith$ | async"
          [email]="sharedWith"
          (remove)="onRemoveSharedWith($event)"></ui-user-pill-badge>
        <input
          data-testid="note-editor-sharedWith"
          class="input shared-with-input"
          type="text"
          [formControl]="emailSearchControl"
          placeholder="Share With"
          clickOutside
          (clickOutside)="closeSuggestionsPicker()" />
      </div>
      <ng-container *ngIf="sharedWithVm$ | async as vm">
        <ui-user-suggestions-dropdown
          [hidden]="vm.hideForm"
          [suggestions]="vm.suggestions"
          (suggestionClicked)="
            onSuggestionClicked($event)
          "></ui-user-suggestions-dropdown>
      </ng-container>
    </form>
  `,
  styles: [
    `
      form {
        height: 100%;
        width: 100%;

        display: flex;
        flex-direction: column;
        align-content: space-around;
      }

      .top-row {
        display: flex;
        align-items: center;

        & input {
          flex-grow: 1;
        }
      }

      .svg-container {
        display: flex;
        align-items: center;
        height: 100%;

        & svg {
          width: auto;
          height: 2rem;
        }

        & #btnDeleteNote svg path {
          fill: var(--color-primary) !important;
        }

        &:hover > #btnDeleteNote svg path {
          fill: var(--color-tertiary) !important;
        }
      }

      .input,
      #sharedWith-container {
        background-color: var(--color-background);
        color: var(--color-primary);
        padding: 5px;
        border: 2px var(--color-tertiary) solid;
        box-shadow: 0 0 15px 4px rgba(0, 0, 0, 0.06);
        border-radius: 10px;
      }

      #sharedWith-container {
        display: flex;
        flex-wrap: wrap;
        align-items: center;

        & > input {
          flex-grow: 1;
          border: none;
        }
      }

      textarea {
        /* flex-grow makes it take the remaining space*/
        flex-grow: 1;
      }

      ::-webkit-scrollbar {
        width: 0.7rem;
      }

      ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 4px var(--color-primary);
        border-radius: 10px;
      }

      ::-webkit-scrollbar-thumb {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 10px var(--color-primary);
      }

      @supports (scrollbar-color: red blue) {
        * {
          scrollbar-color: var(--color-primary);
          scrollbar-width: thin;
        }
      }
    `,
  ],
})
export class NoteEditorComponent implements OnInit {
  formEdit: FormGroup
  @Input() noteToEdit: Note
  @Input() showUIButtons: boolean = true
  @Output() editionCompleted = new EventEmitter<NoteEditorAction>()
  private originalNote: Note
  emailSearchControl = new FormControl('')
  sharedWith$: Observable<string[]>
  hideForm$ = new BehaviorSubject<boolean>(true)
  sharedWithVm$: Observable<{
    suggestions: string[]
    hideForm: boolean
  }>

  constructor(private fb: FormBuilder, private userService: UsersService) {}

  ngOnInit() {
    this.originalNote = deepCopy(this.noteToEdit)

    this.formEdit = this.fb.group({
      id: [this.noteToEdit.id, []],
      userId: [this.noteToEdit.userId, []],
      title: [
        this.noteToEdit.title,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
        ],
      ],
      description: [this.noteToEdit.description, [Validators.maxLength(200)]],
      sharedWith: [this.noteToEdit.sharedWith, []],
    })

    this.sharedWith$ = this.formEdit
      .get('sharedWith')
      .valueChanges.pipe(startWith(this.formEdit.get('sharedWith').value))

    this.sharedWithVm$ = combineLatest([
      this.emailSearchControl.valueChanges.pipe(
        tap(_ => this.hideForm$.next(false)),
        startWith(this.emailSearchControl.value)
      ),
      this.hideForm$.pipe(startWith(true)),
    ]).pipe(
      debounceTime(75),
      switchMap(([searchTerm, hideForm]) =>
        iif(
          () => hideForm || searchTerm == '',
          of({
            suggestions: [],
            hideForm: true,
          }),
          this.userService.getUserSuggestions(searchTerm).pipe(
            map(response => {
              // Only display suggestions of emails that the note is not already shared to
              let sharedWith: string[] = this.formEdit.get('sharedWith').value
              return {
                hideForm: hideForm,
                suggestions: response.filter(
                  suggestion => !sharedWith.includes(suggestion)
                ),
              }
            })
          )
        )
      )
    )
  }

  onClickOutsideEvent() {
    let note: Note = this.formEdit.value
    let editorAction: NoteEditorAction = {
      note: note,
      action:
        !this.formEdit.valid || isDeepEqual(note, this.originalNote)
          ? 'Cancel'
          : 'Update',
    }

    this.editionCompleted.emit(editorAction)
  }

  onDeleteNote() {
    let editorAction: NoteEditorAction = {
      note: this.noteToEdit,
      action: 'Delete',
    }

    this.editionCompleted.emit(editorAction)
  }

  onSuggestionClicked(suggestion: string) {
    let newValue = [...this.formEdit.get('sharedWith').value, suggestion]
    this.formEdit.get('sharedWith').patchValue(newValue)
    this.emailSearchControl.setValue('')
    this.closeSuggestionsPicker()
  }

  onRemoveSharedWith(userEmail) {
    setTimeout(() => {
      let newValue = this.formEdit
        .get('sharedWith')
        .value.filter(sw => sw != userEmail)
      this.formEdit.get('sharedWith').patchValue(newValue)
    }, 0)
  }

  closeSuggestionsPicker() {
    this.hideForm$.next(true)
  }
}
