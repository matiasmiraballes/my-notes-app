import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import {
  byTestId,
  createComponentFactory,
  mockProvider,
  Spectator,
} from '@ngneat/spectator'
import { MockDb } from 'src/app/core/mocks/mock-db/mock-db'
import { Note } from 'src/app/core/models/note.model'
import { NoteEditorAction } from './note-editor-action'
import { NoteEditorComponent } from './note-editor.component'
import { UsersService } from 'src/app/core/http/services/users.service'

let mockNote: Note = new MockDb().Testing.getUserNoteSingle()
class NoteEditorComponentPage {
  constructor(private spectator: Spectator<NoteEditorComponent>) {
    this.spectator = spectator
  }

  public title() {
    return this.spectator.query(byTestId('note-editor-title'))
  }

  public titleText() {
    return this.title()['value']
  }

  public description() {
    return this.spectator.query(byTestId('note-editor-description'))
  }

  public sharedWith() {
    return this.spectator.query(byTestId('note-editor-sharedWith'))
  }

  public submitForm() {
    this.spectator.component.onClickOutsideEvent()
  }
}

describe('NoteEditorComponent', () => {
  let spectator: Spectator<NoteEditorComponent>
  let page: NoteEditorComponentPage

  const createComponent = createComponentFactory({
    component: NoteEditorComponent,
    shallow: true,
    imports: [FormsModule, ReactiveFormsModule],
    providers: [mockProvider(UsersService)],
  })

  beforeEach(() => {
    spectator = createComponent({ props: { noteToEdit: mockNote } })
    page = new NoteEditorComponentPage(spectator)
  })

  it('should autocomplete initial form values with values from the note received as input', () => {
    expect(page.titleText()).toContain(mockNote.title)
  })

  it("should emit an 'editionCompleted' event when the user clicks away from the component", () => {
    let output
    spectator.output('editionCompleted').subscribe(result => {
      output = result
    })

    spectator.component.onClickOutsideEvent()

    expect(output).toExist()
  })

  it('should emit an \'editionCompleted\' event with type "Edit" if the values did change and are valid', () => {
    // Arrange
    let newTitle = mockNote.title + ' Edited'
    let editedMockNote = {
      ...mockNote,
      title: newTitle,
    }
    let expectedOutput: NoteEditorAction = {
      action: 'Update',
      note: editedMockNote,
    }
    let output
    spectator.output('editionCompleted').subscribe(result => {
      output = result
    })

    // Act
    spectator.typeInElement(newTitle, page.title())
    page.submitForm()

    // Assert
    expect(output).toEqual(expectedOutput)
  })

  describe('should emit an \'editionCompleted\' event with type "Cancel" if', () => {
    it("the values didn't change", () => {
      let expectedOutput: NoteEditorAction = {
        action: 'Cancel',
        note: mockNote,
      }
      let output
      spectator.output('editionCompleted').subscribe(result => {
        output = result
      })

      page.submitForm()

      expect(output).toEqual(expectedOutput)
    })

    describe('the values are not valid', () => {
      it('title is too long', () => {
        let newTitle = '123456789012345678901234567890123456789012345678901' // Title must be 3-50 characters long
        let expectedOutput: NoteEditorAction = {
          action: 'Cancel',
          note: {
            ...mockNote,
            title: newTitle,
          },
        }

        let output
        spectator.output('editionCompleted').subscribe(result => {
          output = result
        })

        spectator.typeInElement(newTitle, page.title())
        page.submitForm()

        expect(output).toEqual(expectedOutput)
      })

      it('title is empty', () => {
        let newTitle = '' // Title must be 3-50 characters long
        let expectedOutput: NoteEditorAction = {
          action: 'Cancel',
          note: {
            ...mockNote,
            title: newTitle,
          },
        }

        let output
        spectator.output('editionCompleted').subscribe(result => {
          output = result
        })

        spectator.typeInElement(newTitle, page.title())
        page.submitForm()

        expect(output).toEqual(expectedOutput)
      })
    })
  })
})
