import { Note } from 'src/app/core/models/note.model'

export interface NoteEditorAction {
  note: Note
  action: 'Cancel' | 'Update' | 'Delete'
}
