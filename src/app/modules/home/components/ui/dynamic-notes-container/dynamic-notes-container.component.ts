import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-dynamic-notes-container',
  template: `
    <div #notesContainer class="notes-container" [class]="styleClasses">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
    `
      .notes-container {
        display: grid;
        grid-gap: 15px;
        grid-template-columns: repeat(auto-fill, minmax(18rem, 1fr));
        border-radius: 4px;
        padding: 15px;
      }

      .hasBorder {
        border: solid var(--color-tertiary) 2px;
      }
    `,
  ],
})
export class DynamicNotesContainerComponent implements OnInit, OnChanges {
  @Input() hasBorder: boolean = true
  styleClasses: string = ''

  ngOnInit() {
    this.setStyles()
  }

  ngOnChanges() {
    this.setStyles()
  }

  private setStyles() {
    let styles = ''
    styles += this.hasBorder ? ' hasBorder' : ''
    this.styleClasses = styles
  }
}
