import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-note-frame',
  template: `<div class="note-frame" [class]="styleClasses">
    <ng-content></ng-content>
  </div>`,
  styles: [
    `
      @use 'mixins' as m;

      .note-frame {
        @include m.note-container-size;
        transition: var(--animation-transition-speed);
        border: 1px solid var(--color-primary);
        border-radius: 5px;

        &:hover.onHover {
          border: 1px solid var(--color-tertiary);
          transform: scale(1.05);
        }

        &.highlight {
          border: 1px solid var(--color-tertiary);
        }
      }
    `,
  ],
})
export class NoteFrameComponent implements OnInit, OnChanges {
  @Input() hasOnHoverEffects: boolean = true
  @Input() alwaysHightlighted: boolean = false
  styleClasses: string = ''

  ngOnInit() {
    this.setStyles()
  }

  ngOnChanges() {
    this.setStyles()
  }

  private setStyles() {
    let styles = ''
    styles += this.hasOnHoverEffects ? ' onHover' : ''
    styles += this.alwaysHightlighted ? ' highlight' : ''
    this.styleClasses = styles
  }
}
