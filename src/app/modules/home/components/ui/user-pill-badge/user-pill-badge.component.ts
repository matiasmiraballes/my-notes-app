import { CommonModule } from '@angular/common'
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-user-pill-badge',
  template: ` <div class="badge" [ngClass]="styleClasses" *ngIf="this.email">
    <span>{{ formattedEmail }}</span>
    <input
      *ngIf="hasDelete"
      type="button"
      value="X"
      (click)="onDeleteEmail()" />
  </div>`,
  styles: [
    `
      .badge {
        border: var(--color-tertiary-darkest) solid 2px;
        border-radius: 8px;
        color: var(--color-primary);

        &:hover.onHover {
          color: var(--color-primary-darkest);
        }
      }

      input {
        background-color: var(--color-background);
        color: var(--color-primary);
        border: none;
      }
    `,
  ],
  imports: [CommonModule],
})
export class UserPillBadgeComponent implements OnInit {
  @Input() email: string
  @Input() hasDelete: boolean = true
  @Output() remove = new EventEmitter<string>()
  styleClasses = ''

  ngOnInit() {
    let hoverableEffects = [this.hasDelete]
    let anyHoverableEffectNeeded = hoverableEffects.some(
      actionIsEnabled => actionIsEnabled == true
    )

    this.styleClasses = anyHoverableEffectNeeded ? 'onHover' : ''
  }
  get formattedEmail() {
    return !this.email ? '' : this.email.split('@')[0]
  }

  onDeleteEmail() {
    this.remove.emit(this.email)
  }
}
