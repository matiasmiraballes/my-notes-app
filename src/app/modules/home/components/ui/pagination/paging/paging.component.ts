import { CommonModule } from '@angular/common'
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-paging-control',
  template: `<div class="pagination">
    <span (click)="onClickPage(this.pageSelected - 1)">&laquo;</span>
    <span
      *ngFor="let page of pageArr"
      [class]="this.pageSelected === page ? 'active' : 'notactive'"
      (click)="onClickPage(page)"
      >{{ page }}</span
    >
    <span (click)="onClickPage(this.pageSelected + 1)">&raquo;</span>
  </div>`,
  styles: [
    `
      .pagination {
        display: flex;
        width: 18rem;
        justify-content: center;
      }

      .pagination span {
        color: var[--color-primary];
        float: left;
        padding: 8px 16px;
      }

      .pagination span.active {
        background-color: var(--color-tertiary);
        color: var(--color-primary);
        border-radius: 5px;
      }

      .pagination span:hover:not(.active) {
        background-color: var(--color-tertiary-darkest);
        border-radius: 5px;
      }
    `,
  ],
  imports: [CommonModule],
})
export class PagingComponent implements OnInit, OnChanges {
  @Output() pageChange = new EventEmitter<number>()
  @Input() pages: number = 1
  @Input() pageSelected: number = 1

  pageArr = [1]
  ngOnInit() {
    let { from, to } = this.calculatePagesRange(this.pageSelected, this.pages)

    // generate array [from, ..., to]
    this.pageArr = [...Array(to - from + 1).keys()].map(x => x + from)
    if (this.pageArr.length == 0) this.pageArr = [1]
  }

  ngOnChanges() {
    this.ngOnInit()
  }

  onClickPage(page) {
    if (page >= 1 && page <= this.pages) this.pageChange.emit(page)
  }

  private calculatePagesRange(current: number, total: number) {
    if (total <= 5) return { from: 1, to: total }
    if (current <= 3) return { from: 1, to: 5 }
    if (current >= total - 2) return { from: total - 4, to: total }
    return { from: current - 2, to: current + 2 }
  }
}
