import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core'
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { debounceTime } from 'rxjs'
import { SubSink } from 'subsink'

@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'ui-filter-control',
  template: `
    <input
      data-testid="notes-filter"
      id="name"
      type="text"
      [formControl]="searchTerm"
      placeholder="Search" />
  `,
  styles: [
    `
      input {
        background-color: var(--color-background);
        color: var(--color-primary);
        padding: 5px;
        border: 2px var(--color-tertiary) solid;
        box-shadow: 0 0 15px 4px rgba(0, 0, 0, 0.06);
        border-radius: 10px;
        width: 18rem;
      }
    `,
  ],
  imports: [FormsModule, ReactiveFormsModule],
})
export class FilterControlComponent implements OnInit, OnDestroy {
  private subs = new SubSink()
  @Output() valueChanged = new EventEmitter<string>()
  searchTerm = new FormControl('')

  ngOnInit(): void {
    this.subs.sink = this.searchTerm.valueChanges
      .pipe(debounceTime(200))
      .subscribe(newValue =>
        this.valueChanged.emit(newValue.toLocaleLowerCase())
      )
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }
}
