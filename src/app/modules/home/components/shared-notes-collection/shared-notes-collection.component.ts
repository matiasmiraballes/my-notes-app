import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { PublicNote } from 'src/app/core/models/public-note.model'
import { SharedNotesStore } from 'src/app/core/stores/shared-notes.store'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'home-shared-notes-collection',
  template: `
    <ui-dynamic-notes-container>
      <ng-container *ngFor="let sharedNote of sharedNotes$ | async">
        <ui-shared-note [sharedNote]="sharedNote"></ui-shared-note>
      </ng-container>
    </ui-dynamic-notes-container>
  `,
  styles: [],
  providers: [SharedNotesStore],
})
export class SharedNotesCollectionComponent implements OnInit {
  sharedNotes$: Observable<PublicNote[]>

  constructor(private sharedNotesStore: SharedNotesStore) {}

  ngOnInit(): void {
    this.sharedNotes$ = this.sharedNotesStore.getSharedNotes()
  }
}
