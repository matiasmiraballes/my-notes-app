import {
  byTestId,
  createComponentFactory,
  mockProvider,
  Spectator,
} from '@ngneat/spectator'
import { NoteEditorComponent } from '../ui/note-editor/note-editor.component'
import { NoteCreatorComponent } from './note-creator.component'
import { MockComponent } from 'ng-mocks'
import { NotesStore } from 'src/app/core/stores/notes.store'
import { of } from 'rxjs'
import { fakeAsync, tick } from '@angular/core/testing'

describe('NoteCreatorComponent', () => {
  let spectator: Spectator<NoteCreatorComponent>

  const createComponent = createComponentFactory({
    component: NoteCreatorComponent,
    declarations: [MockComponent(NoteEditorComponent)],
    providers: [
      mockProvider(NotesStore, {
        storeStatus$: of({
          status: 'Initialized',
          error: null,
        }),
      }),
    ],
    shallow: true,
  })

  beforeEach(() => {
    spectator = createComponent()
  })

  it('should display a create button', () => {
    spectator.component.ngOnInit()
    spectator.detectChanges()

    expect(spectator.query(byTestId('create-note-button'))).toBeVisible()
    expect(spectator.query(byTestId('note-editor-form'))).not.toBeVisible()
  })

  it('should display the note creator form instead when the create button is clicked', fakeAsync(() => {
    spectator.component.ngOnInit()
    spectator.detectChanges()

    // Click the 'create note' button and wait 1 ms due to the setTimeout workaround
    spectator.click(byTestId('create-note-button'))
    tick(1)

    spectator.detectChanges()

    expect(spectator.query(byTestId('create-note-button'))).not.toBeVisible()
    expect(spectator.query(byTestId('note-editor-form'))).toBeVisible()
  }))
})
