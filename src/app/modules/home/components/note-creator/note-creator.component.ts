import { NoteEditorAction } from './../ui/note-editor/note-editor-action'
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core'
import { Note } from 'src/app/core/models/note.model'
import { NotesStore } from 'src/app/core/stores/notes.store'
import { BehaviorSubject, combineLatest, filter, map } from 'rxjs'
import { SubSink } from 'subsink'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'note-creator',
  template: `
    <ui-note-frame
      *ngIf="displayPlaceholder$ | async; else loading"
      [hasOnHoverEffects]="(creationFormVisible$ | async) === false">
      <div class="create-note-card" (click)="displayForm()">
        <ng-container
          hidden
          *ngIf="(creationFormVisible$ | async) === false; else noteEditorForm">
          <div class="svg-container">
            <svg-icon
              name="edit-item"
              [class]="'svg-icon'"
              data-testid="create-note-button"
              id="btnCreateNote">
            </svg-icon>
          </div>
        </ng-container>

        <ng-template #noteEditorForm>
          <ui-note-editor
            class="note-editor"
            data-testid="note-editor-form"
            (editionCompleted)="this.createNote($event)"
            [noteToEdit]="emptyNote"
            [showUIButtons]="false">
          </ui-note-editor>
        </ng-template>
      </div>
    </ui-note-frame>
    <ng-template #loading>
      <home-note-placeholder></home-note-placeholder>
    </ng-template>
  `,
  styles: [
    `
      .note-editor {
        height: 100%;
        width: 100%;
      }

      .create-note-card {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        height: 100%;

        &:hover > .svg-container {
          fill: var(--color-tertiary);
        }
      }

      .svg-container {
        width: 9rem;
        transition: var(--animation-transition-speed);
        fill: var(--color-primary);
      }

      /* Thin svg by overwriting part of it with strokes of the same
    color thant the background */
      div svg path {
        stroke: #000000;
        stroke: var(--color-background);
        stroke-width: 1px;
      }
    `,
  ],
})
export class NoteCreatorComponent implements OnInit, OnDestroy {
  subs = new SubSink()
  creationFormVisible$ = new BehaviorSubject<boolean>(false)
  noteCreationInProgress$ = new BehaviorSubject<boolean>(false)
  emptyNote: Note = {
    id: null,
    title: '',
    description: '',
    userId: null,
    sharedWith: [],
  }

  displayPlaceholder$ = combineLatest([
    this.noteCreationInProgress$,
    this.notesStore.storeStatus$,
  ]).pipe(
    map(([creationInProgress, storeStatus]) => {
      return !(creationInProgress && storeStatus.status === 'Updating')
    })
  )

  constructor(private notesStore: NotesStore) {}

  ngOnInit(): void {
    this.subs.sink = this.notesStore.storeStatus$
      .pipe(filter(storeStatus => storeStatus.status === 'Initialized'))
      .subscribe(_ => this.noteCreationInProgress$.next(false))
  }

  createNote(editedNote: NoteEditorAction) {
    this.creationFormVisible$.next(false)

    if (editedNote.action == 'Update') {
      this.noteCreationInProgress$.next(true)
      this.notesStore.createOrUpdateNote(editedNote.note)
    }
  }

  displayForm() {
    // wait for a DOM cycle to avoid closing the form immediately after opening it due to click event being processed for the child component too.
    setTimeout(() => {
      this.creationFormVisible$.next(true)
    }, 0)
  }

  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}
