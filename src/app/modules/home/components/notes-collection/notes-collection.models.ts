import { Note } from 'src/app/core/models/note.model'

export type NoteFrameType = 'Display' | 'Edit' | 'InProgress'

export interface NoteFrame {
  data: Note
  type: NoteFrameType
}
