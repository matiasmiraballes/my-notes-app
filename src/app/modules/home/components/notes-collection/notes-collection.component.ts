import { NoteEditorAction } from './../ui/note-editor/note-editor-action'
import { BehaviorSubject, Observable, combineLatest } from 'rxjs'
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core'
import { Note } from 'src/app/core/models/note.model'
import { filter, map, tap } from 'rxjs/operators'
import { NotesStore } from 'src/app/core/stores/notes.store'
import {
  StoreInfo,
  StoreStatus,
} from 'src/app/core/stores/interfaces/store-info'
import { NoteFrame, NoteFrameType } from './notes-collection.models'
import { SubSink } from 'subsink'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'notes-collection',
  template: `
    <ui-dynamic-notes-container [hasBorder]="false">
      <ui-filter-control
        (valueChanged)="onSearchValueChanged($event)"></ui-filter-control>
      <ui-paging-control
        (pageChange)="this.notesStore.options.set.page($event)"
        [pages]="this.notesStore.options.get.totalPages$ | async"
        [pageSelected]="
          this.notesStore.options.get.page$ | async
        "></ui-paging-control>
    </ui-dynamic-notes-container>
    <ui-dynamic-notes-container data-testid="note-container-background">
      <!-- use ng-container to not mess with grid styling -->
      <ng-container
        *ngIf="
          ['Initialized', 'Updating'].includes((status$ | async).status);
          else loading
        ">
        <note-creator></note-creator>

        <div *ngFor="let noteInstance of notes$ | async">
          <ui-note
            *ngIf="noteInstance.type === 'Display'"
            [note]="noteInstance.data"
            (noteClicked)="onNoteClicked($event)">
          </ui-note>
          <ui-note-frame
            *ngIf="noteInstance.type === 'Edit'"
            [hasOnHoverEffects]="false"
            [alwaysHightlighted]="true">
            <ui-note-editor
              (editionCompleted)="this.createOrUpdateNote($event)"
              [noteToEdit]="noteInstance.data">
            </ui-note-editor>
          </ui-note-frame>
          <home-note-placeholder
            *ngIf="
              noteInstance.type === 'InProgress' &&
              (status$ | async).status === 'Updating'
            "></home-note-placeholder>
        </div>
      </ng-container>
      <ng-template #loading>
        <home-note-placeholder></home-note-placeholder>
      </ng-template>
    </ui-dynamic-notes-container>
  `,
  styles: [],
  providers: [NotesStore],
})
export class NotesCollectionComponent implements OnInit, OnDestroy {
  subs = new SubSink()
  status$: Observable<StoreInfo> = this.notesStore.storeStatus$
  notes$: Observable<NoteFrame[]>
  loadingStatus: StoreStatus
  private noteForEdition: BehaviorSubject<Note> = new BehaviorSubject(null)
  private noteUpdateInProgress: BehaviorSubject<Note> = new BehaviorSubject(
    null
  )

  constructor(public notesStore: NotesStore) {}

  ngOnInit() {
    this.subs.sink = this.notesStore.storeStatus$
      .pipe(
        filter(storeInfo => storeInfo.status === 'Initialized'),
        tap(_ => {
          this.noteUpdateInProgress.next(null)
        })
      )
      .subscribe()

    // If a note is selected for edition, filter it out of the stream, and
    // for that note display a note-editor component instead
    this.notes$ = combineLatest([
      this.notesStore.getNotes(),
      this.noteForEdition.asObservable(),
      this.noteUpdateInProgress.asObservable(),
      this.notesStore.options.get.filter$,
    ]).pipe(
      map(([notesForDisplay, noteForEdition, noteInProgress, searchTerm]) =>
        this.filterBySearchTerm(
          this.getFramesToDisplay(
            notesForDisplay,
            noteForEdition,
            noteInProgress
          ),
          searchTerm
        )
      ),
      map(x => x.sort((a, b) => (a.data?.id > b.data?.id ? 1 : -1)))
    )
  }

  createOrUpdateNote(editorAction: NoteEditorAction) {
    this.noteForEdition.next(null)
    switch (editorAction.action) {
      case 'Update':
        this.noteUpdateInProgress.next(editorAction.note)
        this.notesStore.createOrUpdateNote(editorAction.note)
        break
      case 'Delete':
        this.noteUpdateInProgress.next(editorAction.note)
        this.notesStore.deleteNote(editorAction.note)
        break
      case 'Cancel':
        break
    }
  }

  onNoteClicked(note: Note) {
    setTimeout(() => {
      this.noteForEdition.next(note)
    }, 0)
  }

  onSearchValueChanged(searchTerm: string) {
    this.notesStore.options.set.page(1)
    this.notesStore.options.set.filter(searchTerm)
  }

  private getFramesToDisplay(
    notesToDisplay: Note[],
    noteToEdit: Note,
    noteInProgress: Note
  ): NoteFrame[] {
    if (!notesToDisplay || notesToDisplay.length == 0) return []

    let noteFrames: NoteFrame[] = []
    if (Boolean(noteToEdit))
      noteFrames.push(this.mapToFrameType(noteToEdit, 'Edit'))

    if (Boolean(noteInProgress))
      noteFrames.push(this.mapToFrameType(noteInProgress, 'InProgress'))

    noteFrames = [
      ...noteFrames,
      ...notesToDisplay
        .filter(n => n.id != noteToEdit?.id && n.id != noteInProgress?.id)
        .map(n => this.mapToFrameType(n, 'Display')),
    ]

    return noteFrames
  }

  private mapToFrameType(note: Note, type: NoteFrameType): NoteFrame {
    return {
      data: note,
      type: type,
    }
  }

  private filterBySearchTerm(noteFrames: NoteFrame[], searchTerm: string) {
    if (searchTerm == '') return noteFrames

    return noteFrames.filter(
      nf =>
        nf.data.title.toLocaleLowerCase().includes(searchTerm) ||
        nf.data.description.toLocaleLowerCase().includes(searchTerm)
    )
  }

  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}
