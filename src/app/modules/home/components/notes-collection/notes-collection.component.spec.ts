import { NotesCollectionComponent } from './notes-collection.component'
import { SubscriberSpy, subscribeSpyTo } from '@hirez_io/observer-spy'
import { of } from 'rxjs'
import {
  createComponentFactory,
  mockProvider,
  Spectator,
} from '@ngneat/spectator'
import { MockDb } from 'src/app/core/mocks/mock-db/mock-db'
import { Note } from 'src/app/core/models/note.model'
import { NoteEditorComponent } from '../ui/note-editor/note-editor.component'
import { NoteCreatorComponent } from '../note-creator/note-creator.component'
import { NoteEditorAction } from '../ui/note-editor/note-editor-action'
import { NotesStore } from 'src/app/core/stores/notes.store'
import { NoteFrame, NoteFrameType } from './notes-collection.models'
import { MockComponents } from 'ng-mocks'
import { fakeAsync, tick } from '@angular/core/testing'

let mockNote = new MockDb().Testing.getEmptyNote()
let mockNotes = {
  n1: { ...mockNote, id: 'n1', title: 'Note 1' } as Note,
  n2: { ...mockNote, id: 'n2', title: 'Note 2' } as Note,
  n3: { ...mockNote, id: 'n3', title: 'Note 3' } as Note,
}

describe('NotesCollectionComponent', () => {
  let spectator: Spectator<NotesCollectionComponent>

  const createComponent = createComponentFactory({
    component: NotesCollectionComponent,
    shallow: true,
    declarations: [MockComponents(NoteEditorComponent, NoteCreatorComponent)],
    // 'componentProviders' should be used here to overwrite NotesStore since it was defined in
    // the 'providers' section at component level.
    componentProviders: [
      mockProvider(NotesStore, {
        storeStatus$: of({
          status: 'Initialized',
          error: null,
        }),
        options: {
          get: {
            filter$: of(''),
          },
        },
      }),
    ],
    detectChanges: false, // Change detection must be disabled in order to inject the mock service before ngOnInit is run
  })

  beforeEach(() => {
    spectator = createComponent()
  })

  it('should display all notes on start', () => {
    /* To test ngOnInit service calls we have to turn off detectChanges
        and inject the mock service before running change detection*/
    let inputNotes = [mockNotes.n1, mockNotes.n2, mockNotes.n3]
    let expectedOutput: NoteFrame[] = inputNotes.map(n => {
      return {
        data: n,
        type: 'Display',
      } as NoteFrame
    })

    const resultNotesSpy = InjectMockNotesAndGetSpy(inputNotes)
    expect(resultNotesSpy.getFirstValue()).toEqual(expectedOutput)
  })

  it('should display updated notes on screen if new notes are added after the initial load', () => {
    // Test initial load
    let inputNotes1 = [mockNotes.n1, mockNotes.n2]
    let expectedOutput1: NoteFrame[] = inputNotes1.map(n =>
      noteToFrame(n, 'Display')
    )

    let resultNotesSpy = InjectMockNotesAndGetSpy(inputNotes1)
    expect(resultNotesSpy.getFirstValue()).toEqual(expectedOutput1)

    // Test that notes are updated correcly after initial load
    let inputNotes2 = [...inputNotes1, mockNotes.n3]
    let expectedOutput2 = inputNotes2.map(n => noteToFrame(n, 'Display'))

    let resultNotesSpy2 = InjectMockNotesAndGetSpy(inputNotes2)
    expect(resultNotesSpy2.getFirstValue()).toEqual(expectedOutput2)
  })

  it('should stop displaying a certain note when it is selected for edition', fakeAsync(() => {
    // Load 3 notes at the start
    let inputNotes = [mockNotes.n1, mockNotes.n2, mockNotes.n3]
    let resultNotesSpy = InjectMockNotesAndGetSpy(inputNotes)

    // Simulate user clicking on a note by calling the method triggered by the click event
    // and wait 1ms due to setTimeout workaround
    spectator.component.onNoteClicked(mockNotes.n1)
    tick(1)

    // Expect that note to change to "Edit" mode
    let expectedOutput: NoteFrame[] = [
      noteToFrame(mockNotes.n1, 'Edit'),
      noteToFrame(mockNotes.n2, 'Display'),
      noteToFrame(mockNotes.n3, 'Display'),
    ]
    expect(resultNotesSpy.getValueAt(1)).toEqual(expectedOutput)
  }))

  it('should display the original notes when a note edition get cancelled', fakeAsync(() => {
    // Load 3 notes at the start
    let inputNotes = [mockNotes.n1, mockNotes.n2, mockNotes.n3]
    let resultNotesSpy = InjectMockNotesAndGetSpy(inputNotes)

    // Simulate user clicking on a note by calling the method triggered by the click event
    spectator.component.onNoteClicked(mockNotes.n1)
    tick(1)

    // Expect that note to change to "Edit" mode
    let expectedOutput: NoteFrame[] = [
      noteToFrame(mockNotes.n1, 'Edit'),
      noteToFrame(mockNotes.n2, 'Display'),
      noteToFrame(mockNotes.n3, 'Display'),
    ]
    expect(resultNotesSpy.getValueAt(1)).toEqual(expectedOutput)

    let finalExpectedOutput: NoteFrame[] = [
      noteToFrame(mockNotes.n1, 'Display'),
      noteToFrame(mockNotes.n2, 'Display'),
      noteToFrame(mockNotes.n3, 'Display'),
    ]

    // Simulate a 'cancel edition' event
    let noteEditionEvent: NoteEditorAction = {
      action: 'Cancel',
      note: mockNotes.n1,
    }
    spectator.component.createOrUpdateNote(noteEditionEvent)
    expect(resultNotesSpy.getValueAt(2)).toEqual(finalExpectedOutput)
  }))

  function noteToFrame(note: Note, type: NoteFrameType): NoteFrame {
    return {
      data: note,
      type: type,
    }
  }

  function InjectMockNotesAndGetSpy(inputNotes): SubscriberSpy<NoteFrame[]> {
    // 'fromComponentInjector' flag is used due to NotesStore being injected from
    // the component's provider declaration
    const fromComponentInjector = true
    const notesService = spectator.inject(NotesStore, fromComponentInjector)
    notesService.getNotes.andReturn(of(inputNotes))

    spectator.component.ngOnInit()

    return subscribeSpyTo(spectator.component.notes$)
  }
})
