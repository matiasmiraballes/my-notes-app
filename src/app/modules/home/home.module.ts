import { NoteEditorComponent } from './components/ui/note-editor/note-editor.component'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HomeComponent } from './page/home.component'
import { NotesCollectionComponent } from './components/notes-collection/notes-collection.component'
import { NoteComponent } from './components/ui/note/note.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NoteCreatorComponent } from './components/note-creator/note-creator.component'
import { NotePlaceholderComponent } from './components/ui/note/note.placeholder'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { DirectivesModule } from 'src/app/core/directives/directives.module'
import { SharedNotesCollectionComponent } from './components/shared-notes-collection/shared-notes-collection.component'
import { DynamicNotesContainerComponent } from './components/ui/dynamic-notes-container/dynamic-notes-container.component'
import { UserSuggestionsDropdownComponent } from './components/ui/user-suggestions-dropdown/user-suggestions-dropdown.component'
import { UserPillBadgeComponent } from './components/ui/user-pill-badge/user-pill-badge.component'
import { NoteFrameComponent } from './components/ui/note-frame/note-frame.component'
import { SharedNoteComponent } from './components/ui/shared-note/shared-note.component'
import { FilterControlComponent } from './components/ui/pagination/filter/filter.component'
import { PagingComponent } from './components/ui/pagination/paging/paging.component'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
]

@NgModule({
  declarations: [
    HomeComponent,
    NotesCollectionComponent,
    NoteEditorComponent,
    NoteCreatorComponent,
    NotePlaceholderComponent,
    SharedNotesCollectionComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    DirectivesModule,
    NoteFrameComponent,
    DynamicNotesContainerComponent,
    NoteComponent,
    UserPillBadgeComponent,
    SharedNoteComponent,
    UserSuggestionsDropdownComponent,
    FilterControlComponent,
    PagingComponent,
  ],
})
export class HomeModule {}
