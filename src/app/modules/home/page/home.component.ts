import { ChangeDetectionStrategy, Component } from '@angular/core'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-home',
  template: `
    <div class="container">
      <notes-collection></notes-collection>
      <home-shared-notes-collection></home-shared-notes-collection>
    </div>
  `,
  styles: [
    `
      .container {
        display: grid;
        row-gap: 2.5rem;
      }
    `,
  ],
})
export class HomeComponent {
  constructor() {}
}
