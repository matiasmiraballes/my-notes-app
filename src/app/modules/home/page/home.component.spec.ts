import { Spectator, createComponentFactory } from '@ngneat/spectator'
import { HomeComponent } from './home.component'
import { NotesCollectionComponent } from '../components/notes-collection/notes-collection.component'
import { MockComponents } from 'ng-mocks'
import { SharedNotesCollectionComponent } from '../components/shared-notes-collection/shared-notes-collection.component'

describe('HomeComponent', () => {
  let spectator: Spectator<HomeComponent>

  let createComponent = createComponentFactory({
    component: HomeComponent,
    declarations: [
      MockComponents(SharedNotesCollectionComponent, NotesCollectionComponent),
    ],
  })

  beforeEach(() => {
    spectator = createComponent()
  })

  it('should create component', () => {
    let component = spectator.component
    expect(component).toBeTruthy()
  })
})
