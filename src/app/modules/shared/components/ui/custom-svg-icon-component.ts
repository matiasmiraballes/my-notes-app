import { ChangeDetectionStrategy, Component, Input } from '@angular/core'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'shared-svg-icon',
  template: `
    <div class="svg-container">
      {{ leftLabel }}
      <svg-icon [name]="svgName" [class]="'svg-icon'"></svg-icon>
      {{ rightLabel }}
    </div>
  `,
  styles: [
    `
      .svg-container {
        display: flex;
        align-items: center;
        height: 100%;
        column-gap: 8px;

        & svg {
          width: auto;
          height: 2rem;
        }

        & svg path {
          fill: var(--color-tertiary) !important;
        }
      }
    `,
  ],
})
export class CustomSvgIconComponent {
  @Input() svgName: string = ''
  @Input() leftLabel: string = ''
  @Input() rightLabel: string = ''
}
