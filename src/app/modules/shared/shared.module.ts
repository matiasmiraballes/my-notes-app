import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { CustomSvgIconComponent } from './components/ui/custom-svg-icon-component'

@NgModule({
  declarations: [CustomSvgIconComponent],
  exports: [CustomSvgIconComponent],
  imports: [CommonModule, AngularSvgIconModule],
})
export class SharedModule {}
