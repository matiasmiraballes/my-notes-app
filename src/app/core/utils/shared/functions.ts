/**
 * returns a copy of a deeply nested object
 * @param obj object to copy
 * @returns copy of obj
 */
export function deepCopy(obj: any) {
  if (obj === null || typeof obj !== 'object') {
    return obj
  }
  if (obj instanceof Date) {
    return new Date(obj)
  }
  let copy
  if (obj instanceof Array) {
    copy = []
  } else {
    copy = {}
  }
  let objectsToProcess = [{ source: obj, target: copy }]
  while (objectsToProcess.length > 0) {
    let current = objectsToProcess.pop()
    for (let key in current.source) {
      if (current.source.hasOwnProperty(key)) {
        if (current.source[key] instanceof Object) {
          if (current.source[key] instanceof Array) {
            current.target[key] = []
          } else {
            current.target[key] = {}
          }
          objectsToProcess.push({
            source: current.source[key],
            target: current.target[key],
          })
        } else {
          current.target[key] = current.source[key]
        }
      }
    }
  }
  return copy
}

export const isDeepEqual = (object1, object2) => {
  const objKeys1 = Object.keys(object1)
  const objKeys2 = Object.keys(object2)

  if (objKeys1.length !== objKeys2.length) return false

  for (var key of objKeys1) {
    const value1 = object1[key]
    const value2 = object2[key]

    const isObjects = isObject(value1) && isObject(value2)

    if (
      (isObjects && !isDeepEqual(value1, value2)) ||
      (!isObjects && value1 !== value2)
    ) {
      return false
    }
  }
  return true
}

const isObject = object => {
  return object != null && typeof object === 'object'
}
