import svgData from 'src/assets/svg/svg-data.json'

export interface SvgData {
  path: string
  name: string
}

export class SvgMapper {
  getSvgData(): SvgData[] {
    return svgData
  }
}
