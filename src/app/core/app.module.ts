import { MockRequestInterceptor } from './mocks/interceptors/mock-request.interceptor'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToastrModule } from 'ngx-toastr'

import { AppComponent } from '../root/app.component'
import { AppRoutingModule } from './app-routing.module'

import { AuthModule } from './auth/auth.module'
import { MockBackendService } from './mocks/mock-backend.service'
import { ErrorHandlingModule } from './error-handling/error-handling.module'
import { AngularSvgIconModule } from 'angular-svg-icon'
import { LayoutModule } from '../root/layout/layout.module'
import { SettingsModule } from './settings/settings.module'

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,

    ToastrModule.forRoot(),
    AngularSvgIconModule.forRoot(),
    ErrorHandlingModule,
    LayoutModule,
    AuthModule,
    SettingsModule,
  ],
  providers: [
    MockBackendService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MockRequestInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
