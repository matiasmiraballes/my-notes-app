import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from './auth.service'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-login',
  template: '<p>you are being logged into the application...</p>',
})
export class LoginComponent {
  constructor(private authService: AuthService, router: Router) {
    if (!this.authService.hasValidToken()) {
      this.authService
        .promiseInitializeSingleSignOn()
        .then(() => router.navigateByUrl('/'))
    } else {
      router.navigateByUrl('/')
    }
  }
}
