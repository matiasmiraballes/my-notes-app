export interface IdentityClaims {
  family_name: string
  given_name: string
  name: string
  preferred_username: string
}
