import { IdentityClaims } from './auth.types'
/* eslint-disable brace-style */

import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { OAuthErrorEvent, OAuthService } from 'angular-oauth2-oidc'
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks'
import { BehaviorSubject, combineLatest, Observable, ReplaySubject } from 'rxjs'
import { filter, map } from 'rxjs/operators'
import { ssoConfig } from 'src/environments/sso.config'

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthenticatedSubject$ = new BehaviorSubject<boolean>(false)
  public isAuthenticated$ = this.isAuthenticatedSubject$.asObservable()

  private isDoneLoadingSubject$ = new ReplaySubject<boolean>()
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable()

  /**
   * Publishes `true` if and only if (a) all the asynchronous initial
   * login calls have completed or errorred, and (b) the user ended up
   * being authenticated.
   *
   * In essence, it combines:
   *
   * - the latest known state of whether the user is authorized
   * - whether the ajax calls for initial log in have all been done
   */
  public canActivateProtectedRoutes$: Observable<boolean> = combineLatest([
    this.isAuthenticated$,
    this.isDoneLoading$,
  ]).pipe(map(values => values.every(b => b)))

  private navigateToLoginPage() {
    // TODO: Remember current URL
    this.router.navigateByUrl('/login')
  }

  constructor(private oauthService: OAuthService, private router: Router) {
    // Useful for debugging:
    this.oauthService.events.subscribe(event => {
      if (event instanceof OAuthErrorEvent) {
        console.error('OAuthErrorEvent Object:', event)
      } else {
        console.warn('OAuthEvent Object:', event)
      }
    })

    // This is tricky, as it might cause race conditions (where access_token is set in another
    // tab before everything is said and done there.
    // TODO: Improve this setup. See: https://github.com/jeroenheijmans/sample-angular-oauth2-oidc-with-auth-guards/issues/2
    window.addEventListener('storage', event => {
      // The `key` is `null` if the event was caused by `.clear()`
      if (event.key !== 'access_token' && event.key !== null) {
        return
      }

      console.warn(
        'Noticed changes to access_token (most likely from another tab), updating isAuthenticated'
      )
      this.isAuthenticatedSubject$.next(this.oauthService.hasValidAccessToken())

      if (!this.oauthService.hasValidAccessToken()) {
        this.navigateToLoginPage()
      }
    })

    this.oauthService.events.subscribe(_ => {
      this.isAuthenticatedSubject$.next(this.oauthService.hasValidAccessToken())
    })

    this.oauthService.events
      .pipe(filter(e => ['token_received'].includes(e.type)))
      .subscribe(e => this.oauthService.loadUserProfile())

    this.oauthService.events
      .pipe(
        filter(e => ['session_terminated', 'session_error'].includes(e.type))
      )
      .subscribe(e => this.navigateToLoginPage())

    // this.oauthService.setupAutomaticSilentRefresh();
  }

  public login(targetUrl?: string) {
    // Note: before version 9.1.0 of the library you needed to
    // call encodeURIComponent on the argument to the method.
    this.oauthService.initLoginFlow(targetUrl || this.router.url)
  }

  // initializeSingleSignOn() {
  //   this.oauthService.configure(ssoConfig);
  //   this.oauthService.tokenValidationHandler = new JwksValidationHandler();

  //   this.oauthService.loadDiscoveryDocumentAndTryLogin()
  //   .then(() => {
  //       if (!this.oauthService.hasValidAccessToken()) {
  //           // This will redirect so the promise chain will stop
  //           this.login();
  //       }
  //   })
  //   // .then(() => this.oauthService.loadUserProfile())
  //   .catch(err => console.error('login sequence failed with: ', err));
  // }

  promiseInitializeSingleSignOn(): Promise<void> {
    this.oauthService.configure(ssoConfig)
    this.oauthService.tokenValidationHandler = new JwksValidationHandler()

    return (
      this.oauthService
        .loadDiscoveryDocumentAndTryLogin()
        .then(() => {
          if (!this.oauthService.hasValidAccessToken()) {
            // This will redirect so the promise chain will stop
            return this.login()
          } else {
            this.isAuthenticatedSubject$.next(true)
            return Promise.resolve()
          }
        })
        // .then(() => this.oauthService.loadUserProfile())
        .then(() => {
          if (this.oauthService.hasValidAccessToken()) {
            this.isAuthenticatedSubject$.next(true)
            return Promise.resolve()
          } else {
            this.isAuthenticatedSubject$.next(false)
            return Promise.reject()
          }
        })
        .then(() => {
          this.isDoneLoadingSubject$.next(true)

          // Check for the strings 'undefined' and 'null' just to be sure. Our current
          // login(...) should never have this, but in case someone ever calls
          // initImplicitFlow(undefined | null) this could happen.
          if (
            this.oauthService.state &&
            this.oauthService.state !== 'undefined' &&
            this.oauthService.state !== 'null'
          ) {
            let stateUrl = this.oauthService.state
            if (stateUrl.startsWith('/') === false) {
              stateUrl = decodeURIComponent(stateUrl)
            }
            console.log(
              `There was state of ${this.oauthService.state}, so we are sending you to: ${stateUrl}`
            )
            this.router.navigateByUrl(stateUrl)
          }
        })
        .catch(err => console.error('login sequence failed with: ', err))
    )
  }

  public logout() {
    this.oauthService.logOut()
  }
  public refresh() {
    this.oauthService.silentRefresh()
  }
  public hasValidToken() {
    return this.oauthService.hasValidAccessToken()
  }

  // These normally won't be exposed from a service like this, but
  // for debugging it makes sense.
  public get accessToken() {
    return this.oauthService.getAccessToken()
  }
  public get refreshToken() {
    return this.oauthService.getRefreshToken()
  }
  public get identityClaims() {
    return this.oauthService.getIdentityClaims() as IdentityClaims
  }
  public get idToken() {
    return this.oauthService.getIdToken()
  }
  public get logoutUrl() {
    return this.oauthService.logoutUrl
  }
}
