import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { OAuthModule } from 'angular-oauth2-oidc'
import { LoginComponent } from './login.component'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
]

@NgModule({
  declarations: [LoginComponent],
  imports: [RouterModule.forChild(routes), CommonModule, OAuthModule.forRoot()],
})
export class AuthModule {}
