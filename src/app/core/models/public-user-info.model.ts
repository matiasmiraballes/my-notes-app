export interface PublicUserInfo {
  name: string
  email: string
}
