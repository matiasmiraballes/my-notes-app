import { PublicUserInfo } from './public-user-info.model'

export interface PublicNote {
  id: any
  title: string
  description: string
  owner: PublicUserInfo

  // array of user emails this note has been shared with
  sharedWith: string[]
}
