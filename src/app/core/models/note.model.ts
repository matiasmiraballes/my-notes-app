export interface Note {
  id: any
  title: string
  description: string
  userId: any

  // array of user emails this note has been shared with
  sharedWith: string[]
}
