import { DOCUMENT } from '@angular/common'
import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Inject,
  OnDestroy,
  Output,
} from '@angular/core'
import { filter, fromEvent, Subscription } from 'rxjs'

@Directive({
  selector: '[clickOutside]',
})
export class ClickOutsideDirective implements AfterViewInit, OnDestroy {
  @Output() clickOutside = new EventEmitter<void>()
  documentSubscription: Subscription

  constructor(
    private element: ElementRef,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngAfterViewInit(): void {
    this.documentSubscription = fromEvent(this.document, 'click')
      .pipe(
        filter(event => {
          return this.isTargetClickOutside(event.target as HTMLElement)
        })
      )
      .subscribe(() => {
        this.clickOutside.emit()
      })
  }

  private isTargetClickOutside(elementClicked: HTMLElement): boolean {
    // Check if the element clicked was the current element or one of its childs
    let wasChildClicked =
      elementClicked === this.element.nativeElement ||
      this.element.nativeElement.contains(elementClicked)

    return !wasChildClicked
  }

  ngOnDestroy(): void {
    this.documentSubscription?.unsubscribe()
  }
}
