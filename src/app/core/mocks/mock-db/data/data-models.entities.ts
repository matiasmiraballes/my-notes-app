// These interfaces are only to be used on the context of retrieving objects from the database

export interface UserEntity {
  id: string
  name: string
  email: string
}

export interface NoteEntity {
  id: string
  title: string
  description: string
  userId: string
}

export interface SharedNoteEntity {
  noteId: any
  userId: any
}
