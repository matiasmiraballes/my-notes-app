import users from './secondary-data/mock-users.json'
import notes from './secondary-data/mock-notes.json'
import sharedNotes from './secondary-data/mock-shared-notes.json'
import { DEFAULT_MOCK_API_ID } from '../../settings/models/data-source-settings.model'
import { MockDb } from './mock-db'
import {
  NoteEntity,
  SharedNoteEntity,
  UserEntity,
} from './data/data-models.entities'

export class MockDbFactory {
  private secondaryUsersData: UserEntity[] = users
  private secondaryNotesData: NoteEntity[] = notes
  private secondarySharedNotesData: SharedNoteEntity[] = sharedNotes

  createDb(mockDbId: number) {
    switch (mockDbId) {
      case DEFAULT_MOCK_API_ID:
        return new MockDb()
        break
      case 12:
        return new MockDb(
          [...this.secondaryUsersData],
          [...this.secondaryNotesData],
          [...this.secondarySharedNotesData]
        )
      default:
        return new MockDb()
        break
    }
  }
}
