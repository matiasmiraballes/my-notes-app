//#region import data
import users from './data/mock-users.json'
import notes from './data/mock-notes.json'
import sharedNotes from './data/mock-shared-notes.json'
import {
  NoteEntity,
  SharedNoteEntity,
  UserEntity,
} from './data/data-models.entities'
import { environment } from 'src/environments/environment'
import { Note } from '../../models/note.model'
import { RequestOptions } from '../../stores/interfaces/request-options'
//#endregion

enum DbEntity {
  Note = 'notes',
  User = 'users',
}

export class MockDb {
  private usersData: UserEntity[] = users
  private notesData: NoteEntity[] = notes
  private sharedNotesData: SharedNoteEntity[] = sharedNotes

  //#region Internal Variables
  private idPrefix = {
    notes: 'n',
    users: 'u',
  }

  private idTracker = {
    notes: Math.max(...this.notes().map(obj => parseInt(obj.id.substring(1)))),
    users: Math.max(...this.users().map(obj => parseInt(obj.id.substring(1)))),
  }

  private getNewIdFor(entity: DbEntity) {
    this.idTracker[entity]++
    return `${this.idPrefix[entity]}${this.idTracker[entity]}`
  }
  //#endregion

  /** As having multiple data sources is temporary, I want to keep the
   *  MockDb() constructor valid for ease of use on Unit Tests.
   */
  constructor(usersData?, notesData?, sharedNotesData?) {
    if (usersData != null) {
      this.usersData = usersData
    }
    if (notesData != null) {
      this.notesData = notesData
    }
    if (sharedNotesData != null) {
      this.sharedNotesData = sharedNotesData
    }
  }

  //#region Data collections
  users(): UserEntity[] {
    return this.usersData
  }

  notes(): NoteEntity[] {
    return this.notesData
  }

  sharedNotes(): SharedNoteEntity[] {
    return this.sharedNotesData
  }
  //#endregion

  //#region functions
  getNotes(username, pagintaionOptions?: RequestOptions): Note[] {
    let userId = this.users().find(u => u.email == username).id
    let allNotes = this.notes()
      .filter(n => n.userId == userId)
      .map(note => {
        return {
          ...note,
          sharedWith: this.sharedNotes()
            .filter(sharedNote => sharedNote.noteId == note.id)
            .map(
              swRelationship =>
                this.users().find(u => u.id == swRelationship.userId).email
            ),
        }
      })

    return this.applyFilterAndPagination(allNotes, pagintaionOptions)
  }

  createOrUpdateNote(note: Note): Note {
    if (!note.id || note.id == 0) {
      note.id = this.getNewIdFor(DbEntity.Note)
    }

    let noteEntity: NoteEntity = {
      id: note.id,
      title: note.title,
      description: note.description,
      userId: note.userId,
    }

    this.notesData = [
      noteEntity,
      ...this.notesData.filter(n => n.id != note.id),
    ]

    let sharedNote: SharedNoteEntity[] = note.sharedWith.map<SharedNoteEntity>(
      emailOfSharedWithUser => {
        return {
          noteId: noteEntity.id,
          userId: this.getUserByEmail(emailOfSharedWithUser).id,
        }
      }
    )

    if (sharedNote.length > 0) {
      this.sharedNotesData = [
        ...this.sharedNotesData.filter(sn => sn.noteId != note.id),
        ...sharedNote,
      ]
    }

    return note
  }

  deleteNote(noteId: string) {
    this.notesData = this.notesData.filter(n => n.id != noteId)
  }

  getUserByEmail(email: string) {
    return this.users().find(u => u.email === email)
  }

  getUserSuggestions(searchTerm: string, userEmail): string[] {
    return this.users()
      .filter(u => u.email.includes(searchTerm))
      .filter(u => u.email != userEmail)
      .map(u => u.email)
      .sort()
      .slice(0, 5)
  }

  getUserExists(email: string): boolean {
    return this.users().some(u => u.email == email)
  }
  //#endregion

  //#region Testing Enpoints
  // Helper functions for writing automated tests that provide direct access to the mock data
  Testing = {
    getUserNoteSingle: (): Note => {
      return this.getNotes(environment.mock.mockUserEmail)[0]
    },
    getUserNotes: (): Note[] => {
      return this.getNotes(environment.mock.mockUserEmail)
    },
    getEmptyNote: (): Note => {
      return {
        id: '0',
        title: 'new note',
        description: 'new note description',
        userId: environment.mock.mockUserEmail,
        sharedWith: [],
      }
    },
  }
  //#endregion

  //#region helper functions
  private applyFilterAndPagination(arr: Note[], options?: RequestOptions) {
    let filteredArr = !options?.filter
      ? arr
      : arr.filter(
          n =>
            n.title.toLocaleLowerCase().includes(options.filter) ||
            n.description.toLocaleLowerCase().includes(options.filter)
        )

    if (!options?.limit || options.limit === 0) return filteredArr

    const offset = options.offset ?? 0
    return filteredArr.slice(offset, offset + options.limit)
  }
  //#endregion
}
