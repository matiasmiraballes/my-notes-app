import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { MockBackendService } from '../mock-backend.service'
import { DEFAULT_MOCK_API_ID } from '../../settings/models/data-source-settings.model'
import { DataSourceHeaders } from '../../settings/interceptors/data-source-headers.enum'

@Injectable()
export class MockRequestInterceptor implements HttpInterceptor {
  constructor(private mockBackendService: MockBackendService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let targetHost: string
    try {
      targetHost = new URL(req.url).host
    } catch (error) {
      // Skip mockBackendHandler for non-valid HTTP URLs
      // (ej: 'src/assets/my-file.svg' )
      return next.handle(req)
    }

    // Skip mockBackendHandler if the requests not directed to our backend (ej: authentication)
    if (targetHost != new URL(environment.serviceUri).host) {
      return next.handle(req)
    }

    if (environment.mock.useMocks || this.mockApiIsSelected(req)) {
      return this.mockBackendService.handleRequest(req)
    }

    return next.handle(req)
  }

  private mockApiIsSelected(req: HttpRequest<any>): boolean {
    let selectedSource = req.headers.get(DataSourceHeaders.Source)
    return selectedSource && selectedSource == DEFAULT_MOCK_API_ID.toString()
  }
}
