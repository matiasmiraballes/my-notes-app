import { HttpParams, HttpRequest, HttpResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { catchError, delay, Observable, of, tap, throwError } from 'rxjs'
import { environment } from 'src/environments/environment'
import { GetNotesResponse } from '../http/response/notes/get-notes.response'
import { GetSharedNotesResponse } from '../http/response/shared-notes/get-shared-notes.response'
import { GetSourcesResponse } from '../http/response/sources/get-sources.reponse'
import { GetUserSuggestionsResponse } from '../http/response/users/get-user-suggestion.response'
import { Note } from '../models/note.model'
import { PublicNote } from '../models/public-note.model'
import { DataSourceHeaders } from '../settings/interceptors/data-source-headers.enum'
import {
  RequestOptions,
  RequestOptionsEnum,
} from '../stores/interfaces/request-options'
import { NoteEntity } from './mock-db/data/data-models.entities'
import { MockDb } from './mock-db/mock-db'
import { GetNotesBody } from '../http/body/notes/get-notes.body'
import { MockDbFactory } from './mock-db/mock-db.factory'
import { DEFAULT_MOCK_API_ID } from '../settings/models/data-source-settings.model'

interface Request {
  url: string
  urlWithParams: string
  method: string
  body: any
}

interface EndpointSettings {
  source: number
  delayInMs: number
  unstableConnection: boolean
}

@Injectable()
export class MockBackendService {
  private requestOptionsEnum = RequestOptionsEnum
  private userEmail = environment.mock.mockUserEmail // In real requests, is read from access_token

  /** DISCLAIMER:
   *   Having a secondary source here will only be temporary until the real API is implemented
   *  as there will be no reason to keep two sources of the same kind (Front-End Mock Db).
   *  With that in mind, the factory pattern and the way of handling multiple instances of
   *  mockDb could have been done better for a permanent implementation.
   */

  // Initializing both sources is necessary to not lose the changes between requests
  private mockDbPrimary = new MockDbFactory().createDb(DEFAULT_MOCK_API_ID)
  private mockDbSecondary = new MockDbFactory().createDb(12)
  private mockDb: MockDb

  handleRequest(req: HttpRequest<any>): Observable<any> {
    let endpointUrl = new URL(req.urlWithParams).pathname
    let settings = this.extractDataSourceSettings(req)

    // set the variable reference to the primary or secondary source
    this.mockDb =
      !settings.source || settings.source == DEFAULT_MOCK_API_ID
        ? this.mockDbPrimary
        : this.mockDbSecondary

    let pathHandlers = {
      GET: {
        '/api/read-notes': this.notesGetHandler,
        '/api/read-notes/count': this.notesGetCountHandler,
        '/api/shared-notes': this.sharedNotesGetHandler,
        '/api/users': () => () => new HttpResponse({ status: 404 }),
        '/api/users/suggestions': this.getUserSuggestionsHandler,
        '/api/users/isvalid': this.getUserIsValid,
        '/api/sources': this.sourcesGetHandler,
      },
      POST: {
        '/api/read-notes': this.notesPostHandler,
        '/api/shared-notes': () => new HttpResponse({ status: 404 }),
        '/api/users': () => new HttpResponse({ status: 404 }),
      },
      DELETE: {
        '/api/read-notes': this.notesDeleteHandler,
        '/api/shared-notes': () => new HttpResponse({ status: 404 }),
        '/api/users': () => new HttpResponse({ status: 404 }),
      },
    }

    let handler = pathHandlers[req.method][endpointUrl]
    if (!handler)
      return throwError(
        () =>
          new Error(
            `Handler for mock endpoint ${req.method} ${endpointUrl} is not defined.`
          )
      )

    let params = req.method == 'POST' ? req.body : req.params
    return of(handler(params)).pipe(
      delay(settings.delayInMs),
      tap(() => {
        if (settings.unstableConnection && Math.random() < 0.3) {
          console.warn(
            `Request to endpoint ${req.method} ${endpointUrl} failed due to Unstable Connection' flag. Each individual request will be retried up to 4 times`
          )
          throw new Error(
            `Request to endpoint ${req.method} ${endpointUrl} failed due to 'Unstable Connection' flag.`
          )
        }
      }),
      catchError(error => {
        // catch and re-throw the error so its propagated through the observable pipeline
        return throwError(() => error)
      })
    )
  }

  // Handler methods must be written of as arrow functions
  // so they get strongly binded to the context of the MockBackend instance
  // that way we can use the dictionary notation for pathHandlers
  private notesGetHandler = (
    params: HttpParams
  ): HttpResponse<GetNotesResponse[]> => {
    let pagination = this.extractPaginationOptions(params)
    return new HttpResponse({
      status: 200,
      body: this.mockDb.getNotes(this.userEmail, pagination),
    })
  }

  private notesPostHandler = (body: GetNotesBody) => {
    let noteBody = body.note
    noteBody.userId = this.mockDb.getUserByEmail(this.userEmail).id

    return new HttpResponse<Note>({
      status: 200,
      body: this.mockDb.createOrUpdateNote(noteBody),
    })
  }

  private notesDeleteHandler = (params: HttpParams): HttpResponse<null> => {
    let noteId = params.get('id')
    this.mockDb.deleteNote(noteId)
    return new HttpResponse({
      status: 204,
      body: null,
    })
  }

  private notesGetCountHandler = (params: HttpParams): HttpResponse<number> => {
    let pagination: RequestOptions = {
      ...this.extractPaginationOptions(params),
      limit: 0,
      offset: 0,
    }

    return new HttpResponse({
      status: 200,
      body: this.mockDb.getNotes(this.userEmail, pagination).length,
    })
  }

  private sourcesGetHandler = (
    body: null
  ): HttpResponse<GetSourcesResponse[]> => {
    return new HttpResponse({
      status: 200,
      body: [
        {
          id: 12,
          description: 'Mock API Secondary (Use Front-End Only)',
        },
      ],
    })
  }

  private sharedNotesGetHandler = (
    body: null
  ): HttpResponse<GetSharedNotesResponse[]> => {
    let userId = this.mockDb.users().find(u => u.email == this.userEmail).id

    let notesSharedToUser: NoteEntity[] = this.mockDb
      .sharedNotes()
      .filter(sn => sn.userId == userId)
      .map(sn => this.mockDb.notes().find(n => n.id == sn.noteId))

    let sharedNotes: GetSharedNotesResponse[] = notesSharedToUser.map(note => {
      let noteOwner = this.mockDb.users().find(u => u.id == note.userId)
      let noteOwnerPublicInfo = {
        name: noteOwner.name,
        email: noteOwner.email,
      }
      let noteSharedTo = this.mockDb
        .sharedNotes()
        .filter(sn => sn.noteId == note.id)
        .map(
          currentNoteSharedWith =>
            this.mockDb.users().find(u => u.id == currentNoteSharedWith.userId)
              .email
        )

      let publicNote: PublicNote = {
        id: note.id,
        title: note.title,
        description: note.description,
        owner: noteOwnerPublicInfo,
        sharedWith: noteSharedTo,
      }

      return {
        note: publicNote,
      }
    })

    return new HttpResponse<GetSharedNotesResponse[]>({
      status: 200,
      body: sharedNotes,
    })
  }

  private getUserSuggestionsHandler = (
    params: HttpParams
  ): HttpResponse<GetUserSuggestionsResponse> => {
    return new HttpResponse<GetUserSuggestionsResponse>({
      status: 200,
      body: {
        suggestions: this.mockDb.getUserSuggestions(
          params.get('searchTerm'),
          this.userEmail
        ),
      },
    })
  }

  private getUserIsValid = (params: HttpParams) => {
    let emailToValidate = params.get('email')
    return new HttpResponse<boolean>({
      status: 200,
      body: this.mockDb.getUserExists(emailToValidate),
    })
  }

  //#region Helper Functions
  private extractDataSourceSettings(req: HttpRequest<any>): EndpointSettings {
    return {
      delayInMs: req.headers.get(DataSourceHeaders.Delay)
        ? parseInt(req.headers.get(DataSourceHeaders.Delay))
        : 0,
      unstableConnection:
        req.headers.get(DataSourceHeaders.UnstableConnection) == 'true'
          ? true
          : false,
      source:
        req.headers.get(DataSourceHeaders.Source) != ''
          ? parseInt(req.headers.get(DataSourceHeaders.Source))
          : 11,
    }
  }

  private extractPaginationOptions(params: HttpParams): RequestOptions {
    return {
      limit: params.get(this.requestOptionsEnum.limit)
        ? parseInt(params.get(this.requestOptionsEnum.limit))
        : 8,
      offset: params.get(this.requestOptionsEnum.offset)
        ? parseInt(params.get(this.requestOptionsEnum.offset))
        : 0,
      filter: params.get(this.requestOptionsEnum.filter) ?? '',
    }
  }
  //#endregion
}
