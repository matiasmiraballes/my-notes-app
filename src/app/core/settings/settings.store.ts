import { Injectable } from '@angular/core'
import { ToastrService } from 'ngx-toastr'
import { BehaviorSubject, map, Observable, take, tap, zip } from 'rxjs'
import { DataSourceSettings } from './models/data-source-settings.model'
import {
  StyleSettings,
  ThemeSettings,
  Theme,
} from './models/style-settings.models'
import { DataSourceSettingsService } from './services/data-source-settings.service'
import { StyleSettingsService } from './services/styles-settings.service'

/**
 * This service is associated to the APP_INITIALIZER injection token.
 * The application is not rendered until the initialize() method emits the first value
 */
@Injectable({
  providedIn: 'root',
})
export class SettingsStore {
  private styleSettingsSubject = new BehaviorSubject<StyleSettings>(null)
  styleSettings$ = this.styleSettingsSubject.asObservable()

  private dataSourceSettingsSubject = new BehaviorSubject<DataSourceSettings>(
    null
  )
  dataSourceSettings$ = this.dataSourceSettingsSubject.asObservable()

  initialize(): Observable<any> {
    return zip(
      this.styleSettingsService.get(),
      this.dataSourceSettingsService.get()
    ).pipe(
      take(1),
      tap(([styleSettings, dataSourceSettings]) => {
        this.styleSettingsSubject.next(styleSettings)
        this.dataSourceSettingsSubject.next(dataSourceSettings)
      })
    )
  }

  constructor(
    private styleSettingsService: StyleSettingsService,
    private dataSourceSettingsService: DataSourceSettingsService,
    private toastService: ToastrService
  ) {}

  // Returns only the currently selected theme
  getSelectedTheme(): Observable<Theme> {
    return this.styleSettings$.pipe(
      map(styleSettings => {
        let selectedTheme = this.extractSelectedTheme(styleSettings.theme)
        if (!selectedTheme) this.styleSettingsService.resetToDefault() // Reset to default config if selected theme doesn't exists

        return selectedTheme ?? styleSettings.theme.predefinedThemes[0]
      })
    )
  }

  updateThemesSettings(newStyleSettings: StyleSettings) {
    this.styleSettingsService.update(newStyleSettings)
    this.styleSettingsSubject.next(newStyleSettings)
  }

  updateDataSourceSettings(newDataSourceSettings: DataSourceSettings) {
    this.dataSourceSettingsService.update(newDataSourceSettings)
    this.dataSourceSettingsSubject.next(newDataSourceSettings)
    this.toastService.success('Data Source configuration updated!', '', {
      positionClass: 'toast-top-left',
    })
  }

  private extractSelectedTheme(settings: ThemeSettings): Theme {
    return [...settings.predefinedThemes, ...settings.customThemes].find(
      t => t.id === settings.selectedTheme
    )
  }
}
