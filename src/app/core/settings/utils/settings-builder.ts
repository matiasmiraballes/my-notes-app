import { deepCopy } from '../../utils/shared/functions'

export type DeepNullable<T> = {
  [K in keyof T]: DeepNullable<T[K]> | null
}

export class SettingsBuilder<T> {
  currentSettings: T

  constructor(defaultSettings: T) {
    this.currentSettings = deepCopy(defaultSettings)
  }

  applyOptions(
    options?: DeepNullable<T>,
    appendArrays: boolean = false
  ): SettingsBuilder<T> {
    // Create a copy of currentSettings so the original variable is not modified
    let settingsCopy = deepCopy(this.currentSettings)
    this.currentSettings = this.deepMerge(settingsCopy, options, appendArrays)
    return this
  }

  getConfig() {
    return this.currentSettings
  }

  /**
   * overwrites the values of settings param that exist on options param
   * It's important to note that this function modifies the settings object.
   * @param settings the object to be updated
   * @param options subset of properties to update
   * @param appendArrays when set to true will merge values from arrays instead of overwriting them
   * @returns updated settings
   */
  private deepMerge(settings: T, options?, appendArrays: boolean = false): T {
    for (let key in options) {
      if (options.hasOwnProperty(key)) {
        if (appendArrays && settings[key] && Array.isArray(settings[key])) {
          settings[key] = [...settings[key], ...options[key]]
          continue
        }
        if (settings[key] && typeof settings[key] === 'object') {
          this.deepMerge(settings[key], options[key], appendArrays)
          continue
        }

        // Only overwrite values when option is not null or undefined
        if (Boolean(options[key])) settings[key] = options[key]
      }
    }
    return settings
  }
}
