import { DeepNullable, SettingsBuilder } from './settings-builder'

interface TestObject {
  primitive: string
  nullVariable: null | number
  array: number[]
  object1: {
    primitive: string
    nullVariable: null | number
    array: number[]
  }
  object2: {
    primitive: string
    nullVariable: null | number
    array: number[]
    deeplyNestedObject: {
      primitive: string
      nullVariable: null | number
      array: number[]
    }
  }
}

describe('SettingsBuilder', () => {
  let BASE_OBJECT: TestObject

  beforeEach(() => {
    BASE_OBJECT = {
      primitive: 'a',
      nullVariable: null,
      array: [1, 2, 3],
      object1: {
        array: [4, 5],
        nullVariable: null,
        primitive: 'b',
      },
      object2: {
        primitive: 'c',
        nullVariable: null,
        array: [6, 7],
        deeplyNestedObject: {
          primitive: 'd',
          nullVariable: null,
          array: [8, 9],
        },
      },
    }
  })

  it('all values should be overwrittable', () => {
    let newOptions: DeepNullable<TestObject> = {
      primitive: '2a',
      nullVariable: 32,
      array: [21, 22, 23],
      object1: {
        array: [24, 25],
        nullVariable: 33,
        primitive: '2b',
      },
      object2: {
        primitive: '2c',
        nullVariable: 34,
        array: [26, 27],
        deeplyNestedObject: {
          primitive: 'd',
          nullVariable: 35,
          array: [28, 29],
        },
      },
    }

    let outputSettings = new SettingsBuilder<TestObject>(BASE_OBJECT)
      .applyOptions(newOptions, false)
      .getConfig()

    expect(outputSettings).toEqual(newOptions)
  })

  it('should merge arrays when "appendArrays" flag is true', () => {
    let newOptions: DeepNullable<TestObject> = {
      primitive: '2a',
      nullVariable: 32,
      array: [21, 22, 23],
      object1: {
        array: [24, 25],
        nullVariable: 33,
        primitive: '2b',
      },
      object2: {
        primitive: '2c',
        nullVariable: 34,
        array: [26, 27],
        deeplyNestedObject: {
          primitive: 'd',
          nullVariable: 35,
          array: [28, 29],
        },
      },
    }

    let expectedOptions: TestObject = {
      primitive: '2a',
      nullVariable: 32,
      array: [1, 2, 3, 21, 22, 23],
      object1: {
        array: [4, 5, 24, 25],
        nullVariable: 33,
        primitive: '2b',
      },
      object2: {
        primitive: '2c',
        nullVariable: 34,
        array: [6, 7, 26, 27],
        deeplyNestedObject: {
          primitive: 'd',
          nullVariable: 35,
          array: [8, 9, 28, 29],
        },
      },
    }

    let outputSettings = new SettingsBuilder<TestObject>(BASE_OBJECT)
      .applyOptions(newOptions, true)
      .getConfig()

    expect(outputSettings).toEqual(expectedOptions)
  })

  it('should only overwrite properties that exists on "options" parameter object with non-null values', () => {
    let newOptions: DeepNullable<TestObject> = {
      primitive: undefined,
      nullVariable: 32,
      array: [21, 22, 23],
      object1: {
        primitive: '2b',
        nullVariable: undefined,
        array: undefined,
      },
      object2: {
        primitive: undefined,
        nullVariable: 34,
        array: [26, 27],
        deeplyNestedObject: undefined,
      },
    }

    let expectedOptions: TestObject = {
      primitive: 'a',
      nullVariable: 32,
      array: [21, 22, 23],
      object1: {
        primitive: '2b',
        nullVariable: null,
        array: [4, 5],
      },
      object2: {
        primitive: 'c',
        nullVariable: 34,
        array: [26, 27],
        deeplyNestedObject: {
          primitive: 'd',
          nullVariable: null,
          array: [8, 9],
        },
      },
    }

    let outputSettings = new SettingsBuilder<TestObject>(BASE_OBJECT)
      .applyOptions(newOptions, false)
      .getConfig()

    expect(outputSettings).toEqual(expectedOptions)
  })

  it('should not return a reference of the parameter variables', () => {
    let newOptions: DeepNullable<TestObject> = {
      primitive: '2a',
      nullVariable: 32,
      array: [21, 22, 23],
      object1: {
        array: [24, 25],
        nullVariable: 33,
        primitive: '2b',
      },
      object2: undefined,
    }

    let outputSettings = new SettingsBuilder<TestObject>(BASE_OBJECT)
      .applyOptions(newOptions, false)
      .getConfig()

    expect(BASE_OBJECT).not.toBe(outputSettings)
    expect(BASE_OBJECT.object1).not.toBe(outputSettings.object1)
    expect(BASE_OBJECT.object2).not.toBe(outputSettings.object2)
    expect(BASE_OBJECT.object2.deeplyNestedObject).not.toBe(
      outputSettings.object2.deeplyNestedObject
    )
  })
})
