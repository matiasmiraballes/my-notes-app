import { Injectable } from '@angular/core'
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http'
import { Observable, switchMap } from 'rxjs'
import { SettingsStore } from '../settings.store'
import { DataSourceSettings } from '../models/data-source-settings.model'
import { DataSourceHeaders } from './data-source-headers.enum'

@Injectable()
export class DataSourceInterceptor implements HttpInterceptor {
  constructor(private settingsStore: SettingsStore) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return this.settingsStore.dataSourceSettings$.pipe(
      switchMap((dataSourceSettings: DataSourceSettings) => {
        const source = dataSourceSettings?.selectedSource?.toString() ?? ''
        const delayInMs =
          dataSourceSettings?.options?.delayInMs?.toString() ?? '0'
        const unstableConnection = dataSourceSettings?.options
          ?.unstableConnection
          ? 'true'
          : 'false'

        const newRequest = request.clone({
          headers: request.headers
            .set(DataSourceHeaders.Source, source)
            .set(DataSourceHeaders.Delay, delayInMs)
            .set(DataSourceHeaders.UnstableConnection, unstableConnection),
        })

        return next.handle(newRequest)
      })
    )
  }
}
