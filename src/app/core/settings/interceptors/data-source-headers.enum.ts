export enum DataSourceHeaders {
  Source = 'X-DS-Settings-Source',
  Delay = 'X-DS-Settings-Delay',
  UnstableConnection = 'X-DS-Settings-UnstableConnection',
}
