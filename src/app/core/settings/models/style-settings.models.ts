export const DEFAULT_CUSTOM_THEME_ID: number = 11

export interface Theme {
  id: number
  description: string
  colors: {
    primary: string
    secondary: string
    tertiary: string
    background: string
  }
}

export interface ThemeSettings {
  selectedTheme: number
  customThemes: Theme[]
  predefinedThemes: Theme[]
}

export interface StyleSettings {
  theme: ThemeSettings
}
