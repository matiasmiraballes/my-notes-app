export const DEFAULT_MOCK_API_ID = 11

export interface DataSource {
  id: number
  description: string
}

export interface DataSourceSettings {
  sources: DataSource[]
  selectedSource: number
  options: {
    delayInMs: number
    unstableConnection: boolean
  }
}
