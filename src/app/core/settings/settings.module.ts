import { APP_INITIALIZER, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SettingsStore } from './settings.store'
import { DataSourceSettingsService } from './services/data-source-settings.service'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { DataSourceInterceptor } from './interceptors/data-source.interceptor.ts.interceptor'

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [
    DataSourceSettingsService,
    {
      provide: APP_INITIALIZER,
      multi: true,
      useFactory: (settingsService: SettingsStore) => {
        return () => {
          return settingsService.initialize()
        }
      },
      deps: [SettingsStore],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DataSourceInterceptor,
      multi: true,
    },
  ],
})
export class SettingsModule {}
