import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import {
  LocalStorageService,
  LocalStorageThemeSettings,
} from '../local-storage.service'
import {
  DEFAULT_CUSTOM_THEME_ID,
  StyleSettings,
} from '../models/style-settings.models'
import { SettingsBuilder } from '../utils/settings-builder'
import defaultThemes from '../themes.json'
import { ISectionSettingsService } from './section-settings-service.interface'

@Injectable({
  providedIn: 'root',
})
export class StyleSettingsService
  implements ISectionSettingsService<StyleSettings>
{
  private readonly defaultSettings: StyleSettings = {
    theme: {
      selectedTheme: defaultThemes[0].id,
      predefinedThemes: defaultThemes,
      customThemes: [
        {
          ...defaultThemes[0],
          id: DEFAULT_CUSTOM_THEME_ID,
          description: 'Customize Theme...',
        },
      ],
    },
  }

  constructor(private lsService: LocalStorageService) {}

  /**
   * Returns a settings related to the application styles, defaulting to the default configuration.
   * Initializes localStorage if hasn't been done yet.
   * @returns style section of settings
   */
  get(): Observable<StyleSettings> {
    let styleSettings = new SettingsBuilder<StyleSettings>(this.defaultSettings)
      .applyOptions(this.getFromLocalStorage())
      .getConfig()

    return of(styleSettings)
  }

  update(newSettings: StyleSettings): void {
    let newLocalStorageSettings = this.mapFromSettingsObject(newSettings)
    this.lsService.setLocalStorageThemeSettings(newLocalStorageSettings)
  }

  resetToDefault() {
    this.update(this.defaultSettings)
  }

  private getFromLocalStorage(): StyleSettings {
    let lsSettings = this.lsService.getLocalStorageThemeSettings()
    if (!lsSettings) this.resetToDefault()

    return this.mapFromLsObject(lsSettings)
  }

  private mapFromLsObject(
    localStorageSettings?: LocalStorageThemeSettings
  ): StyleSettings {
    return {
      theme: {
        predefinedThemes: undefined,
        customThemes: localStorageSettings?.customThemes,
        selectedTheme: localStorageSettings?.selectedTheme,
      },
    }
  }

  private mapFromSettingsObject(settings): LocalStorageThemeSettings {
    return {
      selectedTheme: settings.theme.selectedTheme,
      customThemes: settings.theme.customThemes,
    }
  }
}
