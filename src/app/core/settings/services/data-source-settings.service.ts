import { Injectable } from '@angular/core'
import { map, Observable } from 'rxjs'
import { GetSourcesResponse } from '../../http/response/sources/get-sources.reponse'
import { HttpService } from '../../http/services/http-service.service'
import {
  LocalStorageDataSourceSettings,
  LocalStorageService,
} from '../local-storage.service'
import {
  DataSource,
  DataSourceSettings,
  DEFAULT_MOCK_API_ID,
} from '../models/data-source-settings.model'
import { SettingsBuilder } from '../utils/settings-builder'
import { ISectionSettingsService } from './section-settings-service.interface'

@Injectable({
  providedIn: 'root',
})
export class DataSourceSettingsService
  extends HttpService
  implements ISectionSettingsService<DataSourceSettings>
{
  private readonly defaultSettings: DataSourceSettings = {
    sources: [
      { id: DEFAULT_MOCK_API_ID, description: 'Mock API (Use Front-End Only)' },
    ],
    selectedSource: 11,
    options: {
      delayInMs: 0,
      unstableConnection: false,
    },
  }

  API_RESOURCE_ENDPOINT = 'sources'
  constructor(private lsService: LocalStorageService) {
    super()
  }

  get(): Observable<DataSourceSettings> {
    return this.getFromAPI().pipe(
      map(apiOptions => {
        // priority: API > LocalStorage > Default Settings
        let styleSettings = new SettingsBuilder<DataSourceSettings>(
          this.defaultSettings
        )
          .applyOptions(this.getFromLocalStorage())
          .applyOptions(apiOptions, true)
          .getConfig()

        return styleSettings
      })
    )
  }

  update(newSettings: DataSourceSettings) {
    let newLocalStorageSettings = this.mapFromSettingsObject(newSettings)
    this.lsService.setLocalStorageDataSourceSettings(newLocalStorageSettings)
  }

  resetToDefault() {
    this.update(this.defaultSettings)
  }

  private getFromLocalStorage() {
    let lsSettings = this.lsService.getLocalStorageDataSourceSettings()
    if (!lsSettings) this.resetToDefault()

    return this.mapFromLsObject(lsSettings)
  }

  private getFromAPI(): Observable<DataSourceSettings> {
    return this.http
      .get<GetSourcesResponse[]>(this.functionUrl(), {
        headers: this.createHttpHeaders(),
      })
      .pipe(
        map((response: GetSourcesResponse[]) => {
          let sources = response.map<DataSource>(responseSource => {
            return {
              id: responseSource.id,
              description: responseSource.description,
            }
          })

          let availableApiSources: DataSourceSettings = {
            sources: sources,
            selectedSource: undefined,
            options: undefined,
          }

          return availableApiSources
        })
      )
  }

  private mapFromLsObject(
    localStorageSettings?: LocalStorageDataSourceSettings
  ): DataSourceSettings {
    return {
      sources: undefined,
      selectedSource: localStorageSettings?.selectedSource,
      options: {
        delayInMs: localStorageSettings?.options?.delayInMs,
        unstableConnection: localStorageSettings?.options?.unstableConnection,
      },
    }
  }

  private mapFromSettingsObject(
    settings: DataSourceSettings
  ): LocalStorageDataSourceSettings {
    return {
      selectedSource: settings.selectedSource,
      options: {
        delayInMs: settings.options.delayInMs,
        unstableConnection: settings.options.unstableConnection,
      },
    }
  }
}
