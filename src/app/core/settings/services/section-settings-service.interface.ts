import { Observable } from 'rxjs'

export interface ISectionSettingsService<T> {
  get: () => Observable<T>
  update: (newSettings: T) => void
  resetToDefault: () => void
}
