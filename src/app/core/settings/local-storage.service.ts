import { Injectable } from '@angular/core'
import { Theme } from './models/style-settings.models'

export interface LocalStorageThemeSettings {
  selectedTheme: number
  customThemes: Theme[]
}

export interface LocalStorageDataSourceSettings {
  selectedSource: number
  options: {
    delayInMs: number
    unstableConnection: boolean
  }
}

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  private storageKeys = {
    styles: 'styleSettings',
    dataSource: 'dataSource',
    userUUID: 'userUUID',
  }

  constructor() {}

  //#region Theme
  getLocalStorageThemeSettings(): LocalStorageThemeSettings | null {
    let lsThemeSettings = localStorage.getItem(this.storageKeys.styles)
    return lsThemeSettings ? JSON.parse(lsThemeSettings) : null
  }

  setLocalStorageThemeSettings(newSettings: LocalStorageThemeSettings): void {
    localStorage.setItem(this.storageKeys.styles, JSON.stringify(newSettings))
  }
  //#endregion

  //#region DataSource
  getLocalStorageDataSourceSettings(): LocalStorageDataSourceSettings | null {
    let lsDSSettings = localStorage.getItem(this.storageKeys.dataSource)
    return lsDSSettings ? JSON.parse(lsDSSettings) : null
  }

  setLocalStorageDataSourceSettings(
    newSettings: LocalStorageDataSourceSettings
  ): void {
    localStorage.setItem(
      this.storageKeys.dataSource,
      JSON.stringify(newSettings)
    )
  }
  //#endregion

  //#region Logger
  getUserUUID(): string {
    let userCookie = localStorage.getItem(this.storageKeys.userUUID)
    if (Boolean(userCookie)) return userCookie

    let newCookie = crypto.randomUUID()
    localStorage.setItem(this.storageKeys.userUUID, newCookie)
    return newCookie
  }
  //#endregion
}
