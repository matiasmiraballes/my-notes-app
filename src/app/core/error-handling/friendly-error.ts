export class FriendlyError extends Error {
  friendlyMessage: string

  constructor(error, friendlyMessage) {
    super()
    this.message = error.message
    this.name = error.name
    this.stack = error.stack
    this.friendlyMessage = friendlyMessage
  }
}
