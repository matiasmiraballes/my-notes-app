import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { catchError, Observable, retry, throwError, timer } from 'rxjs'

@Injectable()
export class HttpErrorHandlerInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let maxRetries = 4
    // Add a retry strategy with progressive delay between each retry
    return next.handle(req).pipe(
      retry({
        count: maxRetries,
        delay: (_, retryCount) => timer(retryCount * 500),
      }),
      catchError(error => {
        console.error(
          'From HttpErrorHandlerInterceptor',
          `${req.url} failed ${maxRetries} times.`
        )

        // Continue error handling flow when a request failed 'maxRetries' times
        return throwError(() => error)
      })
    )
  }
}
