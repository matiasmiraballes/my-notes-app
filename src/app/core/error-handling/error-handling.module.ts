import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { ErrorHandler, NgModule } from '@angular/core'
import { CustomErrorHandler } from './custom-error-handler.service'
import { HttpErrorHandlerInterceptor } from './http-error-handler.interceptor'

@NgModule({
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorHandlerInterceptor,
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: CustomErrorHandler,
    },
  ],
})
export class ErrorHandlingModule {}
