import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core'
import { ToastrService } from 'ngx-toastr'
import { FriendlyError } from './friendly-error'

@Injectable()
/**
 * Service for handling errors globally
 */
export class CustomErrorHandler implements ErrorHandler {
  constructor(private zone: NgZone, private injector: Injector) {}

  handleError(error: unknown): void {
    if (!(error instanceof Error)) return

    let message =
      error instanceof FriendlyError
        ? error.friendlyMessage
        : 'Something went wrong. For more information check the browser console.'

    // Angular creates ErrorHandler before providers otherwise it won't be able to catch errors that occurs
    // in early phase of application. Hence the providers will not be available to ErrorHandler. So, we need
    // to inject dependent services using injectors.
    let notificationService = this.injector.get(ToastrService)
    console.error('From CustomErrorHandler', error)

    // Since error handling runs outside of Angular Zone, asyncronous code will not trigger change detection
    // Therefore, any asyncronous code must be forced to run within Angular Zone
    this.zone.run(() => {
      notificationService.error(message)
    })
  }
}
