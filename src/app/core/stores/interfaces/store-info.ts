export type StoreStatus =
  | 'NotStarted'
  | 'Loading'
  | 'Initialized'
  | 'Error'
  | 'Updating'

export interface StoreInfo {
  status: StoreStatus
  error: Error | null
}
