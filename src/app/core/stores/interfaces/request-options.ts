import { HttpParams } from '@angular/common/http'

export interface RequestOptions {
  limit: number
  offset: number
  filter: string
}

export enum RequestOptionsEnum {
  'limit' = 'limit',
  'offset' = 'offset',
  'filter' = 'filter',
}

export class RequestOptionsBuilder {
  limit: number
  offset: number
  filter: string

  constructor(limit: number, offset: number, filter: string) {
    this.limit = limit
    this.offset = offset
    this.filter = filter
  }

  asHttpParams() {
    return new HttpParams()
      .set(RequestOptionsEnum.limit, this.limit)
      .set(RequestOptionsEnum.offset, this.offset)
      .set(RequestOptionsEnum.filter, this.filter)
  }

  asObject(): RequestOptions {
    return {
      limit: this.limit,
      offset: this.offset,
      filter: this.filter,
    }
  }
}
