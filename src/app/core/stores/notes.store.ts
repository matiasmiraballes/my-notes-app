import { Injectable, OnDestroy } from '@angular/core'
import { ToastrService } from 'ngx-toastr'
import {
  BehaviorSubject,
  combineLatest,
  Observable,
  switchMap,
  take,
} from 'rxjs'
import { SubSink } from 'subsink'
import { FriendlyError } from '../error-handling/friendly-error'
import { NotesService } from '../http/services/notes.service'
import { Note } from '../models/note.model'
import { DataSourceSettings } from '../settings/models/data-source-settings.model'
import { SettingsStore } from '../settings/settings.store'
import { AbstractStore } from './abstract-store'
import { RequestOptionsBuilder } from './interfaces/request-options'

@Injectable()
export class NotesStore extends AbstractStore implements OnDestroy {
  private subs = new SubSink()

  // Set initial default state
  private notesSubject: BehaviorSubject<Note[]> = new BehaviorSubject([])

  constructor(
    private notesService: NotesService,
    private toastService: ToastrService,
    private settingsStore: SettingsStore
  ) {
    super()
    this.subs.sink = this.notesSubject
      .pipe(switchMap(_ => this.options.get.requestOptions$))
      .subscribe(options => this.updateNoteCount(options))

    this.subs.sink = settingsStore.dataSourceSettings$.subscribe(
      (dataSourceSettings: DataSourceSettings) => {
        this.initialize()
      }
    )
  }

  private updateNoteCount(options) {
    this.notesService.getNotesCount(options).subscribe(count => {
      this.totalItemsSubject.next(count)
    })
  }

  private initialize() {
    this.emitStatus('Loading', null)
    this.getNotesEffect()
  }

  /**
   * Return notes from the store.
   *
   * @type {Observable<Note[]>}
   */
  public getNotes(): Observable<Note[]> {
    return this.notesSubject.asObservable()
  }

  /**
   * Updates notes and updates the state in the store.
   *
   * @type {Observable<Note[]>}
   */
  public createOrUpdateNote(note: Note) {
    this.createOrUpdateNoteEffect(note)
  }

  /**
   * Deletes a note and updates the state in the store.
   *
   * @type {Observable<Note[]>}
   */
  public deleteNote(note: Note) {
    this.deleteNoteEffect(note)
  }

  // #region Effects
  // Effects take care of interacting with the data sources to get/update the data
  // and update the current state (notesSubject) with the results

  // Reducers are also included here, implicit inside the 'next:' functions
  // Reducers are the functions that takes the new piece of data as parameter and returns the updated state

  private getNotesEffect() {
    this.options.get.requestOptions$
      .pipe(
        switchMap(requestOptions =>
          this.notesService.getNotes(requestOptions.asHttpParams())
        )
      )
      .subscribe({
        next: notes => {
          this.notesSubject.next(notes), this.emitStatus('Initialized', null)
        },
        error: (e: Error) => {
          // If fails, the store is set to error status until it is able to successfully retrieve data.
          this.emitStatus('Error', e)

          // The global error handler takes care of notifying the error to the user.
          // The FrienlyError class allow us to re-throw the error with a friendly message
          // while keeping the original message and stack trace intact.
          throw new FriendlyError(e, 'Something failed while loading notes.')
        },
        complete: () => {},
      })
  }

  private createOrUpdateNoteEffect(note: Note) {
    this.emitStatus('Updating', null)
    combineLatest([
      this.notesService.createOrUpdateNote(note),
      this.options.get.requestOptions$,
    ])
      .pipe(take(1))
      .subscribe({
        next: ([createdOrUpdatedNote, requestOptions]) => {
          this.notesSubject.next(
            this.applyFilters(
              [
                createdOrUpdatedNote,
                ...this.notesSubject.value.filter(
                  n => n.id != createdOrUpdatedNote.id
                ),
              ],
              requestOptions
            )
          )
          this.toastService.success('Notes Updated!')
        },
        error: e => {
          // Failing to update notes doesn't lead to a critical failure state
          // so we notify the error and leave the store status as is.
          this.emitStatus(null, e)

          // The global error handler takes care of notifying the error to the user.
          // For this We can re-throw the error with a friendly message.
          throw new FriendlyError(e, 'Something failed while updating notes.')
        },
        complete: () => {
          this.getNotesEffect()
        },
      })
  }

  private deleteNoteEffect(note: Note) {
    this.emitStatus('Updating', null)
    this.notesService.deleteNote(note).subscribe({
      next: () => {
        this.notesSubject.next([
          ...this.notesSubject.value.filter(n => n.id != note.id),
        ])
        this.toastService.success('Note Deleted!')
      },
      error: (e: Error) => {
        // If fails, the store is set to error status until it is able to successfully retrieve data.
        this.emitStatus('Error', e)

        // The global error handler takes care of notifying the error to the user.
        // The FrienlyError class allow us to re-throw the error with a friendly message
        // while keeping the original message and stack trace intact.
        throw new FriendlyError(e, 'Something failed while deleting notes.')
      },
      complete: () => {
        this.getNotesEffect()
      },
    })
  }

  //#endregion

  private applyFilters(
    notes: Note[],
    filterOptions: RequestOptionsBuilder
  ): Note[] {
    let options = filterOptions.asObject()
    return notes
      .filter(
        note =>
          note.title
            .toLocaleLowerCase()
            .includes(options.filter.toLocaleLowerCase()) ||
          note.description
            .toLocaleLowerCase()
            .includes(options.filter.toLocaleLowerCase())
      )
      .slice(0, options.limit)
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe()
  }
}
