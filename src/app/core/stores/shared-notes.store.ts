import { Injectable } from '@angular/core'
import { ToastrService } from 'ngx-toastr'
import { BehaviorSubject, Observable } from 'rxjs'
import { SubSink } from 'subsink'
import { FriendlyError } from '../error-handling/friendly-error'
import { SharedNotesService } from '../http/services/shared-notes.service'
import { PublicNote } from '../models/public-note.model'
import { DataSourceSettings } from '../settings/models/data-source-settings.model'
import { SettingsStore } from '../settings/settings.store'
import { StoreInfo, StoreStatus } from './interfaces/store-info'

@Injectable()
export class SharedNotesStore {
  private subs = new SubSink()

  // Set initial default state
  private sharedNotesSubject: BehaviorSubject<PublicNote[]> =
    new BehaviorSubject([])
  private statusSubject: BehaviorSubject<StoreInfo> = new BehaviorSubject({
    status: 'NotStarted',
    error: null,
  } as StoreInfo)

  constructor(
    private sharedNotesService: SharedNotesService,
    private toastService: ToastrService,
    private settingsStore: SettingsStore
  ) {
    this.subs.sink = settingsStore.dataSourceSettings$.subscribe(
      (dataSourceSettings: DataSourceSettings) => {
        this.initialize()
      }
    )
  }

  private initialize() {
    this.emitStatus('Loading', null)
    this.getSharedNotesEffect()
  }

  public getStoreStatus(): Observable<StoreInfo> {
    return this.statusSubject.asObservable()
  }

  /**
   * Return notes from the store.
   *
   * @type {Observable<PublicNote[]>}
   */
  public getSharedNotes(): Observable<PublicNote[]> {
    return this.sharedNotesSubject.asObservable()
  }

  // #region Effects
  // Effects take care of interacting with the data sources to get/update the data
  // and update the current state (sharedNotesSubject) with the results

  // Reducers are also included here, implicit inside the 'next:' functions
  // Reducers are the functions that takes the new piece of data as parameter and returns the updated state

  private getSharedNotesEffect() {
    this.sharedNotesService.getSharedNotes().subscribe({
      next: (notes: PublicNote[]) => {
        this.sharedNotesSubject.next(notes),
          this.emitStatus('Initialized', null)
      },
      error: (e: Error) => {
        // If fails, the store is set to error status until it is able to successfully retrieve data.
        this.emitStatus('Error', e)

        // The global error handler takes care of notifying the error to the user.
        // The FrienlyError class allow us to re-throw the error with a friendly message
        // while keeping the original message and stack trace intact.
        throw new FriendlyError(
          e,
          'Something failed while loading shared notes.'
        )
      },
      complete: () => {},
    })
  }

  //#endregion

  // Shorthand for emitting new store status, passing null as status argument will leave the current value
  private emitStatus(status: StoreStatus, error: Error) {
    status ??= this.statusSubject.value.status
    this.statusSubject.next({
      status: status,
      error: error,
    })
  }
}
