import { Injectable } from '@angular/core'
import { BehaviorSubject, combineLatest, map, Observable } from 'rxjs'
import { RequestOptionsBuilder } from './interfaces/request-options'
import { StoreInfo, StoreStatus } from './interfaces/store-info'

@Injectable()
export abstract class AbstractStore {
  private statusSubject: BehaviorSubject<StoreInfo> =
    new BehaviorSubject<StoreInfo>({
      status: 'NotStarted',
      error: null,
    })
  storeStatus$ = this.statusSubject.asObservable()

  private filterSubject = new BehaviorSubject<string>('')
  private currentPageSubject = new BehaviorSubject<number>(1)
  private pageSizeSubject = new BehaviorSubject<number>(7)
  protected totalItemsSubject = new BehaviorSubject<number>(0)

  private totalPages$: Observable<number> = combineLatest([
    this.pageSizeSubject,
    this.totalItemsSubject,
  ]).pipe(map(([pageSize, itemCount]) => Math.ceil(itemCount / pageSize)))

  private requestOptions$: Observable<RequestOptionsBuilder> = combineLatest([
    this.currentPageSubject,
    this.pageSizeSubject,
    this.filterSubject,
  ]).pipe(
    map(([page, pageSize, filter]) => {
      return new RequestOptionsBuilder(pageSize, (page - 1) * pageSize, filter)
    })
  )

  options = {
    get: {
      page$: this.currentPageSubject.asObservable(),
      pageSize$: this.pageSizeSubject.asObservable(),
      totalItems$: this.totalItemsSubject.asObservable(),
      filter$: this.filterSubject.asObservable(),
      totalPages$: this.totalPages$,
      requestOptions$: this.requestOptions$,
    },
    set: {
      page: (page: number) => this.currentPageSubject.next(page),
      totalItems: (itemCount: number) => {
        this.totalItemsSubject.next(itemCount)
      },
      filter: (searchTerm: string) => this.filterSubject.next(searchTerm),
      pageSize: (pageSize: number) => this.pageSizeSubject.next(pageSize),
    },
  }

  // Shorthand for emitting new store status, passing null as status argument will leave the current value
  protected emitStatus(status: StoreStatus, error: Error) {
    status ??= this.statusSubject.value.status
    this.statusSubject.next({
      status: status,
      error: error,
    })
  }
}
