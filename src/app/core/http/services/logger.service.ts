import { Injectable } from '@angular/core'
import { LocalStorageService } from '../../settings/local-storage.service'
import { HttpClient } from '@angular/common/http'
import { MetricBody } from '../body/metrics-body'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root',
})
export class LoggerService {
  baseUri = environment.loggerApi.uri
  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService
  ) {}

  logActivity(message: string = '') {
    if (!environment.loggerApi.enabled) return
    let userCookie = this.localStorage.getUserUUID()

    let body: MetricBody = {
      Username: userCookie,
      UserAction: message,
    }

    this.http.post(`${this.baseUri}metrics`, body).subscribe()
  }
}
