import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { createServiceFactory, SpectatorService } from '@ngneat/spectator'
import { MockRequestInterceptor } from '../../mocks/interceptors/mock-request.interceptor'
import { MockDb } from '../../mocks/mock-db/mock-db'
import { Note } from '../../models/note.model'
import { NotesService } from './notes.service'
import { MockBackendService } from '../../mocks/mock-backend.service'
import { subscribeSpyTo } from '@hirez_io/observer-spy'
import { fakeAsync, tick } from '@angular/core/testing'

let mockNotes: Note[] = new MockDb().Testing.getUserNotes()

describe('NotesService', () => {
  let spectator: SpectatorService<NotesService>
  const createService = createServiceFactory({
    service: NotesService,
    imports: [HttpClientModule],
    providers: [
      MockBackendService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: MockRequestInterceptor,
        multi: true,
      },
    ],
  })

  beforeEach(() => (spectator = createService()))

  it("should fetch users' notes", fakeAsync(() => {
    let spyOfGetNotes = subscribeSpyTo(spectator.service.getNotes())

    // The mock backend introduces an artificial delay (of 0ms by default)
    // so we need to tick 1ms before evaluating the result
    tick(1)

    expect(spyOfGetNotes.getFirstValue()).toEqual(mockNotes)
  }))

  it('should add and get new note when createOrUpdateNote is called with a new note', fakeAsync(() => {
    let initialNotesCount = mockNotes.length
    let newNote: Note = new MockDb().Testing.getEmptyNote()
    let newNoteCopy = { ...newNote } // createOrUpdateNote function on MockDb updates the referenced parameter

    let spyOfCreateNote = subscribeSpyTo(
      spectator.service.createOrUpdateNote(newNote)
    )
    let spyOfGetNotes = subscribeSpyTo(spectator.service.getNotes())
    tick(1)

    // Compare prior and new values
    expect(spyOfCreateNote.getFirstValue().title).toEqual(newNoteCopy.title)
    expect(spyOfCreateNote.getFirstValue().description).toEqual(
      newNoteCopy.description
    )

    expect(spyOfGetNotes.getFirstValue().length).toBe(
      Math.min(8, initialNotesCount + 1)
    )
    expect(
      spyOfGetNotes
        .getFirstValue()
        .find(
          n =>
            n.title === newNoteCopy.title &&
            n.description === newNoteCopy.description
        )
    ).toBeDefined()
  }))

  it('should update state when note is updated', fakeAsync(() => {
    let initialNotesCount = mockNotes.length
    let noteToEdit: Note = new MockDb().Testing.getUserNoteSingle()

    let editedNote: Note = {
      ...noteToEdit,
      title: 'edited title',
      description: 'edited description',
    }
    let editedNoteCopy = { ...editedNote }

    let spyOfUpdateNote = subscribeSpyTo(
      spectator.service.createOrUpdateNote(editedNote)
    )
    let spyOfGetNotes = subscribeSpyTo(spectator.service.getNotes())
    tick(1)

    // Compare prior and new values
    expect(spyOfUpdateNote.getFirstValue().title).toEqual(editedNoteCopy.title)
    expect(spyOfUpdateNote.getFirstValue().description).toEqual(
      editedNoteCopy.description
    )

    expect(spyOfGetNotes.getFirstValue().length).toBe(initialNotesCount)
    expect(
      spyOfGetNotes
        .getFirstValue()
        .find(
          n =>
            n.title === editedNoteCopy.title &&
            n.description === editedNoteCopy.description
        )
    ).toBeDefined()
  }))
})
