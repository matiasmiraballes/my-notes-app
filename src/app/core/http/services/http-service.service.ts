import { HttpClient, HttpHeaders } from '@angular/common/http'
import { inject, Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'

@Injectable()
export abstract class HttpService {
  protected abstract API_RESOURCE_ENDPOINT: string
  protected functionUrl = () =>
    `${environment.serviceUri}${this.API_RESOURCE_ENDPOINT}`
  protected http: HttpClient = inject(HttpClient)

  constructor() {}

  protected createHttpHeaders() {
    return new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + sessionStorage['access_token'])
  }
}
