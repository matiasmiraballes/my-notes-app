import { HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { map, Observable, take } from 'rxjs'
import { GetUserSuggestionsResponse } from '../response/users/get-user-suggestion.response'
import { HttpService } from './http-service.service'

@Injectable({
  providedIn: 'root',
})
export class UsersService extends HttpService {
  protected API_RESOURCE_ENDPOINT: string = 'users'
  constructor() {
    super()
  }

  getUserSuggestions(searchTerm: string): Observable<string[]> {
    let params = new HttpParams().set('searchTerm', searchTerm)

    return this.http
      .get<GetUserSuggestionsResponse>(this.functionUrl() + '/suggestions', {
        headers: this.createHttpHeaders(),
        params: params,
      })
      .pipe(
        take(1),
        map(response => response.suggestions)
      )
  }

  isValid(email: string) {
    let params = new HttpParams().set('email', email)

    return this.http.get<boolean>(this.functionUrl() + '/isValid', {
      headers: this.createHttpHeaders(),
      params: params,
    })
  }
}
