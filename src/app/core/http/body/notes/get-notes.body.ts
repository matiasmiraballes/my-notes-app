import { Note } from 'src/app/core/models/note.model'

export interface GetNotesBody {
  note: Note
}
