export interface GetSourcesResponse {
  id: number
  description: string
}
