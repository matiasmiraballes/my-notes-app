export interface GetNotesResponse {
  id: string
  title: string
  description: string

  userId: string
  sharedWith: string[]
}
