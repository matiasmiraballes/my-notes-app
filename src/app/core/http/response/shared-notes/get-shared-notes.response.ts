import { PublicNote } from 'src/app/core/models/public-note.model'

export interface GetSharedNotesResponse {
  note: PublicNote
}
