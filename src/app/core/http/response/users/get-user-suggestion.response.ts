export interface GetUserSuggestionsResponse {
  suggestions: string[]
}
