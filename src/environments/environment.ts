// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mock: {
    useMocks: true, // when true, the application will not try to retrieve sources from the real API
    mockUserEmail: 'user1@mail.com',
  },
  esoConfig: {
    authority: 'https://login.microsoftonline.com/common/v2.0/',
    clientId: '70242385-9754-47aa-8e67-ede870f9de1c',
    // clientId: 'efcadb72-7586-498e-81e0-15a044b917cb',
    redirectUri: 'http://localhost:4200/login',
    issuer:
      'https://login.microsoftonline.com/65fef250-89df-48b7-a2d9-4fe6a7137395/v2.0',
  },
  serviceUri: 'http://localhost:7071/api/',
  loggerApi: {
    uri: 'http://localhost:8080/',
    enabled: false,
  },
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
