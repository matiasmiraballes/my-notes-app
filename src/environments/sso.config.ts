import { environment } from './environment'
import { AuthConfig } from 'angular-oauth2-oidc'

export const ssoConfig: AuthConfig = {
  showDebugInformation: true,

  loginUrl: `${environment.esoConfig.authority}oauth2/v2.0/authorize`,
  logoutUrl: `${environment.esoConfig.authority}oauth2/v2.0/logout`,

  // The SPA's id. The SPA is registerd with this id at the auth-server
  // clientId: 'server.code',
  clientId: environment.esoConfig.clientId,

  // dummyClientSecret: '3.k36iHryJ0rg~C~--40G0gRl9r_0c.WUY',

  oidc: true,

  // URL of the SPA to redirect the user to after login
  redirectUri: environment.esoConfig.redirectUri,

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  // scope: 'openid profile email offline_access api',
  scope: 'openid profile email offline_access',

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',
  // responseType: 'code',
  responseType: 'id_token token',

  // Url of the Identity Provider
  // issuer: 'https://idsvr4.azurewebsites.net',
  issuer: environment.esoConfig.issuer,

  // silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  // useSilentRefresh: true, // Needed for Code Flow to suggest using iframe-based refreshes
  // silentRefreshTimeout: 5000, // For faster testing
  // timeoutFactor: 0.25, // For faster testing

  skipIssuerCheck: false,
  clearHashAfterLogin: true,
  strictDiscoveryDocumentValidation: false,
}
