export const environment = {
  production: false,
  mock: {
    useMocks: true,
    mockUserEmail: 'user1@mail.com',
  },
  esoConfig: {
    authority: 'https://login.microsoftonline.com/common/v2.0/',
    clientId: 'efcadb72-7586-498e-81e0-15a044b917cb',
    redirectUri: 'https://mynotes-mm.netlify.app/login',
    issuer:
      'https://login.microsoftonline.com/65fef250-89df-48b7-a2d9-4fe6a7137395/v2.0',
  },
  serviceUri: 'http://localhost:7071/api/',
  loggerApi: {
    uri: 'https://test-minimal-api-xv5fs6jrbq-uc.a.run.app/',
    enabled: true,
  },
}
